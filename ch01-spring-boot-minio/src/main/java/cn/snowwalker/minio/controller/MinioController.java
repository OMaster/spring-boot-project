package cn.snowwalker.minio.controller;

import java.io.InputStream;
import java.util.List;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import cn.snowwalker.minio.service.MinioService;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author snowwalker
 * @date 2023/12/9
 */
@RestController
public class MinioController {

    @Resource
    private MinioService minioService;

    private String bucketName = "blog";

    @GetMapping("/hasBucket")
    public boolean hasBucket(String bucketName) {
        return minioService.hasBucket(bucketName);
    }

    /**
     * 创建存储桶
     * @param bucketName 存储桶名称
     */
    @GetMapping("/createBucket")
    public void createBucket(String bucketName) {
        minioService.createBucket(bucketName);
    }

    /**
     * 删除存储桶
     * @param bucketName 存储桶名称
     */
    @GetMapping("/deleteBucket")
    public void deleteBucket(String bucketName) {
        minioService.deleteBucket(bucketName);
    }

    /**
     * 获取存储桶列表
     * @return
     */
    @GetMapping("/getBuckets")
    public List<Bucket> getBuckets() {
        return minioService.getBuckets();
    }


    /**
     * 上传文件
     * @param file
     * @return
     */
    @GetMapping("/upload")
    public String uploadFile(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        try (InputStream inputStream = file.getInputStream()) {
            return minioService.uploadFile(bucketName, fileName, file.getContentType(), inputStream);
        } catch (Exception ex) {
            return "文件上传失败";
        }
    }

    @GetMapping("/download")
    public void downloadFile(String bucketName, String fileName, HttpServletResponse response) {
        try (ServletOutputStream os = response.getOutputStream()) {
            InputStream inputStream = minioService.downloadFile(bucketName, fileName);
//            inputStream.transferTo(os);
            FileCopyUtils.copy(inputStream, os);
//            os.write(FileCopyUtils.copyToByteArray(inputStream));
        } catch (Exception ex) {
        }
    }

    /**
     * 获取文件访问地址
     * @param bucketName 存储桶名称
     * @param fileName   文件名称
     * @return
     */
    @GetMapping("/getFileUrl")
    public String getFileUrl(String bucketName, String fileName) throws Exception {
        return minioService.getFileUrl(bucketName, fileName);
    }

    /**
     * 获取文件列表
     * @return
     */
    @GetMapping("/listObjects")
    public List<Item> listObjects() {
        return minioService.listObjects();
    }

    /**
     * 删除文件
     * @param fileName 文件名称
     */
    @GetMapping("/remove")
    public void remove(String fileName) throws Exception {
        minioService.deleteFile(bucketName, fileName);
    }
}

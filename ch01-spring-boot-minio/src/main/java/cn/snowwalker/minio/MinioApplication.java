package cn.snowwalker.minio;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author snowwalker
 * @date 2023/12/9
 */
@SpringBootApplication
public class MinioApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MinioApplication.class, args);

        Arrays.stream(run.getBeanDefinitionNames()).forEach(System.out::println);
    }
}

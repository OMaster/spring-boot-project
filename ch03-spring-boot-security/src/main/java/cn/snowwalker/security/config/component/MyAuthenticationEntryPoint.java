package cn.snowwalker.security.config.component;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.response.ResultBack;
import cn.orangebright.common.util.JsonUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * 未登录用户处理逻辑
 * @author OMaster
 * @date 2022/9/17
 */
@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ResultBack<Object> resultBack = ResultBack.unauthorized("未登录");
        JsonUtil.writeObjectToResponse(resultBack, response);
    }
}

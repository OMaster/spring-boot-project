package cn.snowwalker.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * @author snowwalker
 * @date 2024/1/18
 */
@Configuration
public class UserDetailsConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
        // 需要支持短信验证码登录，故账号必须为手机号
        // 声明 PasswordEncoder 类型的bean后，需要用声明的bean来加密密码
        UserDetails userDetails = User.withUsername("2806103144@qq.com")
//                .password("{noop}123")
                .password(passwordEncoder.encode("123"))
                .roles(new String[]{"add"})
                .build();
        return new InMemoryUserDetailsManager(userDetails);
    }
}

package cn.snowwalker.security.config.verificationcode.email;

import cn.snowwalker.security.config.verificationcode.VerificationCodeLoginConfigurer;

/**
 * @author snowwalker
 * @date 2024/1/18
 */
//@Component
public class EmailLoginConfigurer extends VerificationCodeLoginConfigurer {

    public EmailLoginConfigurer() {
        super(new EmailAuthenticationFilter());
    }

}

package cn.snowwalker.security.config.verificationcode;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.util.Assert;

/**
 * 验证码认证器：短信验证码、邮箱验证码等
 * @author snowwalker
 * @date 2024/1/17
 */
public class VerificationCodeAuthenticationProvider implements AuthenticationProvider, InitializingBean {

    private StringRedisTemplate redisTemplate;

    private UserDetailsService userDetailsService;

    private UserCache userCache = new NullUserCache();

    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.redisTemplate, "A StringRedisTemplate must be set");
        Assert.notNull(this.userDetailsService, "A UserDetailsService must be set");
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String key = determineKey(authentication);
        boolean cacheWasUsed = true;
        UserDetails user = this.userCache.getUserFromCache(key);
        if (user == null) {
            cacheWasUsed = false;

            String smsCode = redisTemplate.opsForValue().get(key);
            if (smsCode == null) {
                throw new BadCredentialsException("验证码已过期");
            }
            if (!smsCode.equals(authentication.getCredentials())) {
                throw new BadCredentialsException("验证码错误");
            }

            // 需要支持使用手机号或邮箱进行数据查询
            user = userDetailsService.loadUserByUsername(key);

            if (user == null) {
                throw new UsernameNotFoundException("用户不存在");
            }
        }
        if (!cacheWasUsed) {
            this.userCache.putUserInCache(user);
        }
        return createSuccessAuthentication(user, authentication, user);
    }

    private String determineKey(Authentication authentication) {
        return (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
    }

    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication,
                                                         UserDetails user) {
        // Ensure we return the original credentials the user supplied,
        // so subsequent attempts are successful even with encoded passwords.
        // Also ensure we return the original getDetails(), so that future
        // authentication events after cache expiry contain the details
        UsernamePasswordAuthenticationToken result = UsernamePasswordAuthenticationToken.authenticated(principal,
                authentication.getCredentials(), this.authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());
        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public StringRedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}

package cn.snowwalker.security.config.sms;

import jakarta.annotation.Resource;

import cn.snowwalker.security.config.component.SelfAuthenticationSuccessHandler;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author snowwalker
 * @date 2024/1/18
 */
@Deprecated
//@Component
public class SmsLoginConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    @Resource(name = "stringRedisTemplate")
    private StringRedisTemplate redisTemplate;

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private SelfAuthenticationSuccessHandler authenticationSuccessHandler;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        SmsAuthenticationFilter smsAuthenticationFilter = new SmsAuthenticationFilter();
        smsAuthenticationFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
        smsAuthenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
//        smsAuthenticationFilter.setAuthenticationFailureHandler(authenticationFailureHandler);
        http.authenticationProvider(getSmsAuthenticationProvider());
        http.addFilterBefore(smsAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    public SmsAuthenticationProvider getSmsAuthenticationProvider() {
        SmsAuthenticationProvider smsAuthenticationProvider = new SmsAuthenticationProvider();
        smsAuthenticationProvider.setRedisTemplate(redisTemplate);
        smsAuthenticationProvider.setUserDetailsService(userDetailsService);
        return smsAuthenticationProvider;
    }
}

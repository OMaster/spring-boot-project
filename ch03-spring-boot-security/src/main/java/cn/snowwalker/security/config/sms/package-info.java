/**
 * 单个短信验证码配置，使用最新的 verificationcode 包下的配置
 * @author snowwalker
 * @date 2024/1/30
 */
package cn.snowwalker.security.config.sms;
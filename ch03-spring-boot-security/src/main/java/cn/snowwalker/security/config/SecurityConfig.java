package cn.snowwalker.security.config;

import jakarta.annotation.Resource;

import cn.snowwalker.security.config.component.SelfAuthenticationSuccessHandler;
import cn.snowwalker.security.config.verificationcode.VerificationCodeAuthenticationProvider;
import cn.snowwalker.security.config.verificationcode.VerificationCodeLoginConfigurer;
import cn.snowwalker.security.config.verificationcode.email.EmailLoginConfigurer;
import cn.snowwalker.security.config.verificationcode.sms.SmsLoginConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author snowwalker
 * @date 2024/1/17
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Resource(name = "stringRedisTemplate")
    private StringRedisTemplate redisTemplate;

    @Resource
    private UserDetailsService userDetailsService;

    @Resource
    private SelfAuthenticationSuccessHandler authenticationSuccessHandler;

    @Bean
    public VerificationCodeLoginConfigurer smsLoginConfigurer() {
        return new SmsLoginConfigurer();
    }

    @Bean
    public EmailLoginConfigurer emailLoginConfigurer() {
        return new EmailLoginConfigurer();
    }

    /**
     * 获取 AuthenticationManager 对象，需要容器中存在 UserDetailsService类型的bean对象
     * @param configuration 认证配置对象
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(req -> req
                        .requestMatchers("/login", "/login/sms", "/login/email").permitAll()
                        .anyRequest().authenticated()
                )
                .formLogin(login -> login.successHandler(authenticationSuccessHandler))
                .logout(Customizer.withDefaults())
        ;

        // 配置验证码认证器
        http.authenticationProvider(getVerificationCodeAuthenticationProvider());
        // 配置短信验证码登录
        http.apply(smsLoginConfigurer());
        // 配置邮箱验证码登录
        http.apply(emailLoginConfigurer());

        return http.build();
    }

    /**
     * 如果自定义一个 AuthenticationProvider 类型的bean，会导致默认的 DaoAuthenticationProvider 失效。
     * 如果定义多个，则不会添加到 AuthenticationManager中
     * @see org.springframework.security.config.annotation.authentication.configuration.InitializeAuthenticationProviderBeanManagerConfigurer.InitializeAuthenticationProviderManagerConfigurer
     * @see org.springframework.security.config.annotation.authentication.configuration.InitializeUserDetailsBeanManagerConfigurer.InitializeUserDetailsManagerConfigurer
     */
    private VerificationCodeAuthenticationProvider getVerificationCodeAuthenticationProvider() {
        VerificationCodeAuthenticationProvider verificationCodeAuthenticationProvider = new VerificationCodeAuthenticationProvider();
        verificationCodeAuthenticationProvider.setRedisTemplate(redisTemplate);
        verificationCodeAuthenticationProvider.setUserDetailsService(userDetailsService);
        return verificationCodeAuthenticationProvider;
    }

}

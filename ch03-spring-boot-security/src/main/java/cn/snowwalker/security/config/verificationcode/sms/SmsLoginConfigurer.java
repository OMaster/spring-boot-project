package cn.snowwalker.security.config.verificationcode.sms;

import cn.snowwalker.security.config.verificationcode.VerificationCodeLoginConfigurer;

/**
 * @author snowwalker
 * @date 2024/1/18
 */
//@Component
public class SmsLoginConfigurer extends VerificationCodeLoginConfigurer {

    public SmsLoginConfigurer() {
        super(new SmsAuthenticationFilter());
    }

}

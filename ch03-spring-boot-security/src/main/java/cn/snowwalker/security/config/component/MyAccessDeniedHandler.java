package cn.snowwalker.security.config.component;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.response.ResultBack;
import cn.orangebright.common.util.JsonUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

/**
 * @author OMaster
 * @date 2022/9/17
 */
@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResultBack<Object> resultBack = ResultBack.forbidden("无权限访问");
        JsonUtil.writeObjectToResponse(resultBack, response);
    }
}

package cn.snowwalker.security.config.component;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.response.ResultBack;
import cn.orangebright.common.util.JsonUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * 退出登录过滤器
 *
 * @author OMaster
 * @date 2022/9/13
 */
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        ResultBack<Object> resultBack = ResultBack.success("注销成功");
        JsonUtil.writeObjectToResponse(resultBack, response);
    }
}

package cn.snowwalker.security.config.sms;

import java.io.Serial;

import org.springframework.security.core.AuthenticationException;

/**
 * 短信验证码认证异常
 * @author snowwalker
 * @date 2024/1/17
 */
@Deprecated
public class SmsAuthenticationException extends AuthenticationException {

    @Serial
    private static final long serialVersionUID = -7020331624838817978L;

    public SmsAuthenticationException(String message) {
        super(message);
    }
}

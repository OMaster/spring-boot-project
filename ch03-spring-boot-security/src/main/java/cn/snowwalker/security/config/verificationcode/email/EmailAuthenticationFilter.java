package cn.snowwalker.security.config.verificationcode.email;

import jakarta.servlet.http.HttpServletRequest;

import cn.snowwalker.security.config.verificationcode.AbstractVerificationCodeAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * 短信验证码认证过滤器
 * @author snowwalker
 * @date 2024/1/17
 */
public class EmailAuthenticationFilter extends AbstractVerificationCodeAuthenticationFilter {

    public static final String SPRING_SECURITY_FORM_EMAIL_KEY = "email";

    public static final String SPRING_SECURITY_FORM_CODE_KEY = "code";

    private static final AntPathRequestMatcher DEFAULT_ANT_PATH_REQUEST_MATCHER = new AntPathRequestMatcher("/login/email",
            "POST");

    private String emailParameter = SPRING_SECURITY_FORM_EMAIL_KEY;

    private String codeParameter = SPRING_SECURITY_FORM_CODE_KEY;

    public EmailAuthenticationFilter() {
        this(DEFAULT_ANT_PATH_REQUEST_MATCHER);
    }

    public EmailAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    public EmailAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    @Override
    protected String obtainKey(HttpServletRequest request) {
        return request.getParameter(this.emailParameter);
    }

    @Override
    protected String obtainCode(HttpServletRequest request) {
        return request.getParameter(this.codeParameter);
    }

    public String getEmailParameter() {
        return emailParameter;
    }

    public void setEmailParameter(String emailParameter) {
        this.emailParameter = emailParameter;
    }

    public String getCodeParameter() {
        return codeParameter;
    }

    public void setCodeParameter(String codeParameter) {
        this.codeParameter = codeParameter;
    }
}

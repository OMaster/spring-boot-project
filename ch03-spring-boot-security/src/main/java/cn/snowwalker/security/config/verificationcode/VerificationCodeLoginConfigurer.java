package cn.snowwalker.security.config.verificationcode;

import jakarta.annotation.Resource;

import cn.snowwalker.security.config.component.SelfAuthenticationFailureHandler;
import cn.snowwalker.security.config.component.SelfAuthenticationSuccessHandler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 验证码登录配置
 * @author snowwalker
 * @date 2024/1/29
 */
public class VerificationCodeLoginConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private AbstractVerificationCodeAuthenticationFilter authFilter;

    @Resource
    private SelfAuthenticationSuccessHandler successHandler;

    @Resource
    private SelfAuthenticationFailureHandler failureHandler;

    public VerificationCodeLoginConfigurer(AbstractVerificationCodeAuthenticationFilter authFilter) {
        this.authFilter = authFilter;
    }

    public VerificationCodeLoginConfigurer(AbstractVerificationCodeAuthenticationFilter authFilter,
                                           SelfAuthenticationSuccessHandler successHandler,
                                           SelfAuthenticationFailureHandler failureHandler) {
        this.authFilter = authFilter;
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        authFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
        authFilter.setAuthenticationSuccessHandler(successHandler);
        authFilter.setAuthenticationFailureHandler(failureHandler);
        AbstractVerificationCodeAuthenticationFilter filter = this.postProcess(authFilter);
        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }


    public AbstractVerificationCodeAuthenticationFilter getAuthFilter() {
        return authFilter;
    }

    public void setAuthFilter(AbstractVerificationCodeAuthenticationFilter authFilter) {
        this.authFilter = authFilter;
    }

    public SelfAuthenticationSuccessHandler getSuccessHandler() {
        return successHandler;
    }

    public void setSuccessHandler(SelfAuthenticationSuccessHandler successHandler) {
        this.successHandler = successHandler;
    }

    public AuthenticationFailureHandler getFailureHandler() {
        return failureHandler;
    }

    public void setFailureHandler(SelfAuthenticationFailureHandler failureHandler) {
        this.failureHandler = failureHandler;
    }
}

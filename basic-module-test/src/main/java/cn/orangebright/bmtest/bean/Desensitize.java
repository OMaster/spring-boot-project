package cn.orangebright.bmtest.bean;

import cn.hutool.core.util.DesensitizedUtil;
import cn.orangebright.common.desensitize.core.regex.annotation.EmailDesensitize;
import cn.orangebright.common.desensitize.core.slider.annotation.ChineseNameDesensitize;
import cn.orangebright.common.desensitize.core.slider.annotation.IdCardDesensitize;
import cn.orangebright.common.desensitize.core.slider.annotation.MobileDesensitize;
import cn.orangebright.common.desensitize.core.slider.annotation.PasswordDesensitize;
import lombok.Data;

/**
 * 脱敏，或者直接使用hutool工具类{@link cn.hutool.core.util.DesensitizedUtil#desensitized(CharSequence, DesensitizedUtil.DesensitizedType)}
 * @author snowwalker
 * @date 2024/3/5
 */
@Data
public class Desensitize {

    @ChineseNameDesensitize
    private String name;

    @MobileDesensitize
    private String phone;

    @IdCardDesensitize
    private String idCard;

    @EmailDesensitize
    private String email;

    @PasswordDesensitize
    private String password;
}

package cn.orangebright.bmtest.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.file.FileUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2024/2/5
 */
@RestController
@RequestMapping("/export")
public class ExportFileController {

    @GetMapping("/re")
    public void exportResource(HttpServletRequest request, HttpServletResponse response) {
        String filename = "教师资格名单模板.xlsx";
        FileUtil.exportResourceFile(filename, response);
    }

    @GetMapping("/re2")
    public void exportResource2(HttpServletRequest request, HttpServletResponse response) {
        String filename = "pdf/Java开发手册（黄山版）.pdf";
        FileUtil.exportResourceFile(filename, response);
    }

    @GetMapping("/re3")
    public void exportResource3(HttpServletRequest request, HttpServletResponse response) {
        String filename = "pdf/Java开发手册（黄山版）.pdf";
        FileUtil.exportTemplate(filename, response);
    }

    @GetMapping("str")
    public String str() {
        return "export-string";
    }
}

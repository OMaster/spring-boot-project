package cn.orangebright.bmtest.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.file.FileUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2024/4/18
 */
@RestController
public class ExportController {

    @GetMapping("/ico")
    public void ico(HttpServletRequest request, HttpServletResponse response) {
        FileUtil.exportResourceFile("favicon.ico", response);
    }

    @GetMapping("/file")
    public void file(String filePath, HttpServletResponse response) {
        FileUtil.exportFile(filePath, response);
    }
}

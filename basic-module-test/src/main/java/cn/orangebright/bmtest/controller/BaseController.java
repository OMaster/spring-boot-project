package cn.orangebright.bmtest.controller;

import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.bmtest.bean.Desensitize;
import cn.orangebright.common.bean.PageVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2024/2/27
 */
@RestController
public class BaseController {

    @GetMapping
    public Object a403a(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return null;
    }

    @GetMapping("ex")
    public Object ex(HttpServletResponse response) {
        throw new RuntimeException("模拟异常");
    }

    @GetMapping("/pv")
    public PageVo pageVo(PageVo pageVo) {
        System.out.println(pageVo);
        return pageVo;
    }

    @GetMapping("/rp")
    public String rp(@RequestParam String name) {
        System.out.println(name);
        return name;
    }

    @PostMapping("/poj")
    public PageVo poj(@RequestBody PageVo pageVo) {
        System.out.println(pageVo);
        return pageVo;
    }

    /**
     * 脱敏
     * @return
     */
    @GetMapping("/tm")
    public Object desensitize() {
        Desensitize desensitize = new Desensitize();
        desensitize.setName("陈子明");
        desensitize.setPhone("15621462957");
        desensitize.setEmail("obmaster@126.com");
        desensitize.setPassword("obmaster");
        desensitize.setIdCard("429006199808200036");
        return desensitize;
    }
}

package cn.orangebright.bmtest.controller;

import cn.orangebright.common.response.PageResult;
import cn.orangebright.spring.handler.GlobalResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2024/2/19
 */
@GlobalResponse
@RestController
@RequestMapping("global-back")
public class GlobalBackController {

    @GetMapping("str")
    public String str() {
        return "global-back";
    }

    @GetMapping("null")
    public String nul() {
        return null;
    }

    @GetMapping("obj")
    public Object obj() {
        PageResult pr = new PageResult();
        pr.setTotal(102);
        return pr;
    }


    @GetMapping(value = "type", produces = MediaType.IMAGE_PNG_VALUE)
    public String type() {
        return "type";
    }

}

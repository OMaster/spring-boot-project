package cn.orangebright.bmtest;

import cn.orangebright.spring.EnableBasicConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author snowwalker
 * @date 2024/2/5
 */
@EnableBasicConfig
@SpringBootApplication
public class BasicModuleTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicModuleTestApplication.class, args);
    }
}

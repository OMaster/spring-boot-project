package cn.snowwalker.client01.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2023/12/16
 */
@RestController
public class IndexController {

    @GetMapping("/")
    public String home() {
        return "首页";
    }

    /**
     * 有权限访问，会自动给我们设置的scope添加SCOPE_前缀
     * @see org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService
     */
    @GetMapping("/a/")
    @PreAuthorize("hasAuthority('SCOPE_profile')")
    public String homea() {
        return "首页";
    }

    /**
     * 无权限访问
     * @return
     */
    @GetMapping("/index")
    @PreAuthorize("hasAuthority('profile')")
    public String index() {
        return "欢迎xxx，登录成功！";
    }

}

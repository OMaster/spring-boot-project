package cn.snowwalker.client01.config;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @author snowwalker
 * @date 2023/12/16
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(requests -> requests
//                        .requestMatchers("/").permitAll()
                        .anyRequest().authenticated())
//                .oauth2Login(Customizer.withDefaults())
                .exceptionHandling(handle -> handle.accessDeniedHandler((request, response, accessDeniedException) -> response.getOutputStream().write("权限不足".getBytes(StandardCharsets.UTF_8))))
//                .oauth2ResourceServer((resourceServer) -> resourceServer
//                        .jwt(jwt -> jwt.jwtAuthenticationConverter(null)));
                .oauth2Login(Customizer.withDefaults())
                .oauth2Client(Customizer.withDefaults())
                .logout(logout -> logout.logoutSuccessHandler(oidcLogoutSuccessHandler())
//                        (request, response, authentication) -> {
//                    // TODO 2024/1/8 增加对认证中心的注销
//                    System.out.println("注销成功");
//                    response.getOutputStream().write("zxcg".getBytes(StandardCharsets.UTF_8));
//                })
                )
//                .oidcLogout((logout) -> logout
//                        .backChannel(Customizer.withDefaults())
//                )
        ;
//        DefaultLoginPageGeneratingFilter sharedObject = http.getSharedObject(DefaultLoginPageGeneratingFilter.class);
//        sharedObject.setOauth2LoginEnabled(true);
        return http.build();
    }

    private LogoutSuccessHandler oidcLogoutSuccessHandler() {
        OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler =
                new OidcClientInitiatedLogoutSuccessHandler(this.clientRegistrationRepository);

        // Sets the location that the End-User's User Agent will be redirected to
        // after the logout has been performed at the Provider
        oidcLogoutSuccessHandler.setPostLogoutRedirectUri("https://www.baidu.com");

        return oidcLogoutSuccessHandler;
    }
}

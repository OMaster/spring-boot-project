package cn.snowwalker.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author snowwalker
 * @date 2023/12/16
 */
@SpringBootApplication
public class AuthenticationCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationCenterApplication.class, args);
    }
}

package cn.snowwalker.authentication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 认证类
 * @author snowwalker
 * @date 2023/12/19
 */
@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    @GetMapping("/")
    @ResponseBody
    public String index() {
        return "欢迎来到统一认证中心";
    }

    @GetMapping("login")
    public String login() {
        return "login";
    }

    @PostMapping("/index")
    @ResponseBody
    public String ind() {
        return "successForwardUrl";
    }
}

package cn.snowwalker.authentication.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author snowwalker
 * @date 2023/12/16
 */
@RestController
public class LoginController {

    @GetMapping("")
    public String index() {
        return "登录成功";
    }

    @GetMapping("logout1")
    public String logout() {
        return "注销成功";
    }
}

package cn.orangebright.common.util;

import org.junit.jupiter.api.Test;
import org.lionsoul.ip2region.xdb.Searcher;
import org.springframework.core.io.ClassPathResource;

/**
 * 地区工具测试类
 *
 * @author snowwalker
 * @date 2023/7/3
 */
public class RegionUtilTest {

    @Test
    void getRegion() {
        String ip = "111.203.250.46";
        String region = RegionUtil.getRegion(ip);
        System.out.println(region);
    }

    @Test
    void ip2region() {
        String ip = "111.203.250.46";
        System.out.println(one(ip));
    }

    public String one(String ip) {
        ClassPathResource cpr = new ClassPathResource("ip2region.xdb");
        try {
            String dbPath = cpr.getFile().getPath();
            // 1、从 dbPath 中预先加载 VectorIndex 缓存，并且把这个得到的数据作为全局变量，后续反复使用。
            byte[] vIndex = Searcher.loadVectorIndexFromFile(dbPath);
            // 2、使用全局的 vIndex 创建带 VectorIndex 缓存的查询对象。
            Searcher searcher = Searcher.newWithVectorIndex(dbPath, vIndex);
            // 3、查询
            String region = searcher.search(ip);
            // 4、关闭资源
            searcher.close();
            return region;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "XX XX";
    }

}

package cn.orangebright.common.util;

import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2021/12/2
 */
class RandomUtilTest {

    @Test
    void randomInt() {
        System.out.println(RandomUtil.randomInt(40) + 10);
        System.out.println(RandomUtil.randomInt(50));
        System.out.println(RandomUtil.randomInt());
    }

    private static final ThreadLocalRandom RANDOM = RandomUtil.getRandom();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Player().start();
//            new Thread(() -> System.out.println(Thread.currentThread().getName() + ": " + RandomUtil.randomInt(100))).start();
        }
    }

    private static class Player extends Thread {
        @Override
        public void run() {
//            System.out.println(getName() + ": " + RandomUtil.randomInt(100));
            ThreadLocalRandom random = RandomUtil.getRandom();
            System.out.println(getName() + ": " + random.nextInt(100));
        }
    }
}
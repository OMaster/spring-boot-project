package cn.orangebright.common.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.SecretKey;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2022/8/13
 */
class JwtUtilTest {

    @Test
    void jwtTest() {
        // github示例
        SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        String jws = Jwts.builder().setSubject("Joe").setIssuer("OMaster").signWith(key).compact();
        Claims rs1 = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jws).getBody();
        System.out.println(rs1);

        Map<String, Object> claims = new HashMap<>();
        claims.put("username", "omaster");
//        claims.put("type", "admin");
//        String token = createToken(claims, null, null);
//        System.out.println(token);
//
//        Claims body = verifyToken(token, null);
//        if (body == null) {
//            System.out.println("token错误");
//        } else {
//            System.out.println(body.get("username"));
//            System.out.println(body.getSubject());
//            System.out.println(body.getIssuer());
//            System.out.println(body.getExpiration());
//            System.out.println(body);
//        }
//
//        String s = "1";
//        token = createToken(null, s, null);
//        System.out.println(token);
//
//        body = verifyToken(token, s);
//        System.out.println(body);
    }

    @Test
    void createNoExpirationToken() {
        System.out.println(new Date(1663772563));
    }

    @Test
    void createToken() {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", "omaster");
        claims.put("type", "admin");
        String token = JwtUtil.createToken(claims, null, 1 * 60);
        System.out.println(token);

        token = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNjYzNzcyNTYzLCJpc3MiOiJPTWFzdGVyIn0.VsB1JEtWpUJtIma870235BuPxOi5X-YtH8OZkWT4pcU";
        Claims claim = JwtUtil.verifyToken(token);
        System.out.println(claim.getExpiration());
    }

    @Test
    void testCreateToken() {
    }

    @Test
    void testCreateToken1() {
    }

    @Test
    void verifyToken() {
    }
}
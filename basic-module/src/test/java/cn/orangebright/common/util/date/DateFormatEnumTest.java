package cn.orangebright.common.util.date;

import java.util.Date;

import org.junit.jupiter.api.Test;

/**
 * @author snowwalker
 * @date 2024/2/22
 */
class DateFormatEnumTest {

    @Test
    void threadLocalObject() {
        System.out.println(DateFormatEnum.getSimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date()));
    }
}
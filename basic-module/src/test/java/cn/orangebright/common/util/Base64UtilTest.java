package cn.orangebright.common.util;

import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2023/6/16
 */
class Base64UtilTest {

    @Test
    void decode() {
        String str = "踏雪者ob";

        String s = Base64Util.encodeToString(str);
        System.out.println(s);

        System.out.println(Base64Util.decodeToString(s));

    }
}
package cn.orangebright.common.util;

import jakarta.servlet.http.HttpServletRequest;

import cn.orangebright.common.request.RequestUtil;
import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2023/4/11
 */
class RequestUtilTest {

    @Test
    void getRequestAttributes() {
    }

    @Test
    void getHttpServletRequest() {
        HttpServletRequest httpServletRequest = RequestUtil.getRequest();
    }

    @Test
    void getHttpServletResponse() {
    }

    @Test
    void getRequestIp() {
    }

    @Test
    void getRemoteIp() {
    }
}
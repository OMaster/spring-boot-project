package cn.orangebright.common.util;

import cn.orangebright.common.response.PageResult;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2022/8/1
 */
class JsonUtilTest {

    PageResult pageEntity = new PageResult();
    String json;

    @BeforeEach
    void before(){
        pageEntity.setPageIndex(1);
        pageEntity.setPageSize(100);
        pageEntity.setDataList(null);

        json = JsonUtil.objectToString(pageEntity);
        System.out.println(json);
    }

    @Test
    void jsonToObject() {
        PageResult bean = JsonUtil.jsonToObject(json, PageResult.class);
        System.out.println(bean);
        System.out.println();
    }

    @Test
    void jsonToTree() {
        JsonNode jsonNode = JsonUtil.jsonToTree(json);
        System.out.println(jsonNode);
        System.out.println(jsonNode.get("pageSize"));
    }

    @Test
    void objectToTree() {
        JsonNode jsonNode = JsonUtil.objectToTree(pageEntity);
        System.out.println(jsonNode);
        System.out.println(jsonNode.get("currentPage"));
    }

    @Test
    void treeToObject() {
        JsonNode jsonNode = JsonUtil.jsonToTree(json);
        System.out.println(jsonNode);
        System.out.println(jsonNode.get("pageSize"));

        PageResult bean = JsonUtil.treeToObject(jsonNode, PageResult.class);
        System.out.println(bean);
    }
}
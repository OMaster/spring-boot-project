package cn.orangebright.common.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.junit.jupiter.api.Test;
import org.springframework.util.FileCopyUtils;

/**
 * @author orangebright
 * @date 2023/6/5
 */
class ImageUtilTest {

    @Test
    void compress() throws Exception {
        String path = "C:\\Users\\Snowwalker\\Desktop\\踏雪者.png";
        FileInputStream fis = new FileInputStream(path);
        byte[] compress = ImageUtil.compress(fis, 0.7, 0.8f);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\Snowwalker\\Desktop\\txz.jpg");
        FileCopyUtils.copy(compress, fos);
    }

    @Test
    void testCompress() {
    }

    @Test
    void compress1080() {
    }

    @Test
    void testCompress1() {
    }

    @Test
    void testCompress2() {
    }


//    public static void main(String[] args) throws Exception {
//        String path = "C:\\Users\\CHSI\\Desktop\\压缩测试\\a.jpg";
////        compress(path, 1, 1);
////        Thread.sleep(5000);
////        compress(path, 0.5, 1);
////        Thread.sleep(5000);
////        compress(path, 1, 0.9F);
//
////        byte[] bytes = FileCopyUtils.copyToByteArray(new File(path));
////        compress(bytes);
//
//        compress(path, 1, 0.9f);
//        compress(path, 0.9f);
//    }
//
//    public static void compress(String path, double scale, float quality) throws IOException {
//        Thumbnails.of(path).scale(scale).outputQuality(quality).toFile("C:\\Users\\CHSI\\Desktop\\压缩测试\\" + RandomUtil.randomInt() + ".jpg");
//    }
//
//    public static void compress(String path, float quality) throws IOException {
//        Thumbnails.of(path).size(1920, 1080).outputQuality(quality).toFile("C:\\Users\\CHSI\\Desktop\\压缩测试\\" + RandomUtil.randomInt() + ".jpg");
//    }
//
//    public static void compress(byte[] file) throws IOException {
//        while (file.length > 300 * 1024) {
//            System.out.println("第几次压缩");
//            file = compress(file, 1, 0.6f);
//        }
//        File file1 = new File("C:\\Users\\CHSI\\Desktop\\压缩测试\\" + RandomUtil.randomInt() + ".jpg");
//        FileCopyUtils.copy(file, file1);
//    }
}
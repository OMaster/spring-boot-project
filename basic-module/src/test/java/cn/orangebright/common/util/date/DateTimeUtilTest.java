package cn.orangebright.common.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2021/12/17
 */
class DateTimeUtilTest {

    Date date = new Date();
    LocalDateTime localDateTime = LocalDateTime.of(2021, 12, 17, 19, 23, 34);

    @Test
    void one() {
        /**
         *  LocalDate：日期值对象如2019-09-22
         *  LocalTime：时间值对象如21:25:36
         *  LocalDateTime：日期+时间值对象
         *  Zoned：时区
         *  Zoned DateTime：日期+时间+时区值对象
         *  DateTime Formatter：用于日期时间的格式化
         *  Period：用于计算日期间隔
         *  Duration：用于计算时间间隔
         */

        //使用 of方法创建他们的实例，如下创建一个 东八区 2020-8-10 10:14:40的时间对象
        LocalDate localDate = LocalDate.of(2020, 8, 10);
        LocalTime localTime = LocalTime.of(10, 14, 40);
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        System.out.println(localDate);
        System.out.println(localTime);
        System.out.println(localDateTime);
        System.out.println(zonedDateTime);

        /***********************************************************************************************
         *                              LocalDate, LocalTime, LocalDateTime
         **********************************************************************************************/

        //获取当前日期
        LocalDate now = LocalDate.now();
        System.out.println(now);
        //获取当前日期的各个部分
        int year = now.getYear();
        int month = now.getMonthValue();
        int day = now.getDayOfMonth();
        System.out.println("Today is " + year + "/" + month + "/" + day);
        System.out.println(now.getDayOfYear());
        System.out.println(now.getMonth());

        LocalDate ld = LocalDate.of(2020, 8, 8);
        System.out.println(ld.getDayOfYear());

        //修改当前日期，返回一个修改后的日期对象
        LocalDate newDate = now.withYear(1998);
        System.out.println(newDate);

        //获取当前时间
        LocalTime time = LocalTime.now();
        System.out.println(time);
        System.out.println(time.toString());
        //获取时间的各个部分
        int h = time.getHour();
        int m = time.getMinute();
        int s = time.getSecond();
        int n = time.getNano();
        System.out.println("Taday time is " + h + ":" + m + ":" + s + "." + n);

        //日期时间
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt);

        //获取本地时区
        ZoneId zonId = ZoneId.systemDefault();
        System.out.println(zonId);

        /***********************************************************************************************
         *                            Period, Duration 用于计算时间间隔
         **********************************************************************************************/
        //创建一个两周的间隔
        Period periodWeeks = Period.ofWeeks(2);
        System.out.println(periodWeeks);

        //一年三个月零三天的间隔
        Period custom = Period.of(1, 3, 2);

        //一天的时长
        Duration duration = Duration.ofDays(1);

        //计算1998.8.20到现在过了多久
        LocalDate birthday = LocalDate.of(1998, 8, 20);
        Period between = Period.between(now, birthday);
        System.out.println(between);
        System.out.println("Your age is " + between.getYears() + "年" + between.getMonths() + "月" + between.getDays() + "天");

        /***********************************************************************************************
         *                            与Date, Calendar的转换
         **********************************************************************************************/

        //LocalDateTime 转 Date
        Date localDateTimeDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        System.out.println("LocalDateTime is  " + localDateTime);
        System.out.println("    date      is  " + localDateTimeDate);
        //LocalDateTime 转 Calendar
        Calendar localDateTimeCalendar = GregorianCalendar.from(ZonedDateTime.of(localDateTime, ZoneId.systemDefault()));
        System.out.println(localDateTimeCalendar);

        Date date = new Date();
        System.out.println(date);

        //Date 转 LocalDateTime
        LocalDateTime dateLocalDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        System.out.println(dateLocalDateTime);
    }

    @Test
    void dateToLocalDateTime() {
        System.out.println(date);
        System.out.println(DateTimeUtil.dateToLocalDateTime(date));
        System.out.println(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }

    @Test
    void localDateTimeToDate() {
        System.out.println(localDateTime);
        System.out.println(DateTimeUtil.localDateTimeToDate(localDateTime));
    }


    @Test
    void dateFormat() throws ParseException {
        System.out.println(date);
        System.out.println(DateTimeUtil.dateFormat(date));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss SSS");
        System.out.println(simpleDateFormat.parse("2021/12/11 13:32:23 315"));
    }

    @Test
    void testDateFormat() {
        System.out.println(DateTimeUtil.dateFormat(date, "yyyy/MM/dd hh:mm:ss SSS"));
    }

    @Test
    void localDateTimeFormat() {
        System.out.println(DateTimeUtil.localDateTimeFormat(localDateTime));
    }

    @Test
    void testLocalDateTimeFormat() {
        System.out.println(DateTimeUtil.localDateTimeFormat(localDateTime, "yyyy/MM/dd hh:mm:ss SSS"));
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDateTime.atZone(ZoneId.systemDefault())));
    }

    @Test
    void parseToDate() {
        System.out.println(DateTimeUtil.parseToDate("2021-02-1 2:2:02", "yyyy-MM-dd HH:mm:ss"));
        System.out.println(DateTimeUtil.parseToDate("2021/12/11 12:32:20 315", "yyyy-MM-dd HH:mm:ss SSS"));
        System.out.println(DateTimeUtil.parseToDate("12:32:20 315", "HH:mm:ss SSS"));
    }

    @Test
    void parseLocalDateTime() {
        System.out.println(DateTimeUtil.parseToLocalDateTime("2021/02/01 03:32:02 315", "yyyy/MM/dd HH:mm:ss SSS"));
        System.out.println(DateTimeUtil.parseToLocalDateTime("2021/12/11 13:32:23 315", "yyyy/MM/dd HH:mm:ss SSS"));
    }

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * SimpleDateFormat 多线程下问题demo
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Date date = new Date(10000000 + i * 1000);
            new Thread(() -> {
                SimpleDateFormat simpleDateFormat = DateFormatEnum.getSimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String format = sdf.format(date);
//                String format = simpleDateFormat.format(date);
                System.out.println(format);
                try {
                    // parse() 方法使用 sdf 报错（多线程问题）
                    // 使用ThreadLocal获取的对象正常，每个线程互不影响
                    Date parse = sdf.parse(format);
//                    Date parse = simpleDateFormat.parse(format);
                    System.out.println(parse);
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }
}
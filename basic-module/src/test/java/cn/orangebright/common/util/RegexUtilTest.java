package cn.orangebright.common.util;

import org.junit.jupiter.api.Test;

/**
 * @author snowwalker
 * @date 2024/2/27
 */
class RegexUtilTest {

    @Test
    void isPhone() {
        String phone = "11621462957";
        System.out.println(RegexUtil.isValidPhone(phone));

        phone = "14621462957";
        System.out.println(RegexUtil.isValidPhone(phone));
    }

    @Test
    void isEmail() {
        String email = "a@b.c";
        System.out.println(RegexUtil.isValidEmail(email));

        email = "a@bd.cd";
        System.out.println(RegexUtil.isValidEmail(email));
    }
}
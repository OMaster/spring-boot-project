package cn.orangebright.common.util;

import org.junit.jupiter.api.Test;

/**
 * @author orangebright
 * @date 2023/6/16
 */
class RsaUtilTest {

    @Test
    void generateKeyPair() {
        RsaUtil.Base64KeyPair base64KeyPair = RsaUtil.generateKeyPair();

        String str = "admin1234";
        String encrypt = RsaUtil.encrypt(str, base64KeyPair.getPublicKey());
        System.out.println("加密字符串：\n" + encrypt);
        String decrypt = RsaUtil.decrypt(encrypt, base64KeyPair.getPrivateKey());
        System.out.println("解密字符串：" + decrypt);

        str = "踏雪者ob";
        String encrypt1 = RsaUtil.encrypt(str, base64KeyPair.getPublicKey());
        System.out.println("加密字符串：\n" + encrypt1);
        String decrypt1 = RsaUtil.decrypt(encrypt1, base64KeyPair.getPrivateKey());
        System.out.println("解密字符串：" + decrypt1);
    }
}
package cn.orangebright.common.file;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.util.FileCopyUtils;

/**
 * @author snowwalker
 * @date 2024/2/2
 */
class ZipUtilTest {

    @Test
    void compress() throws Exception {
        File file = new File("C:\\Users\\CHSI\\Desktop\\迎新春趣味运动会报名表@陈子明.xlsx");
        FileInputStream fis = new FileInputStream(file);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\CHSI\\Desktop\\test.zip");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] bytes = FileCopyUtils.copyToByteArray(file);

        List<ZipUtil.FileEntry> fileEntries = Arrays.asList(new ZipUtil.FileEntry("报名表.xlsx", bytes), new ZipUtil.FileEntry("abc", null));

        ZipUtil.compress(fos, fileEntries);

        byte[] byteArray = baos.toByteArray();
        System.out.println(Arrays.toString(byteArray));
    }
}
package cn.orangebright.common.file;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author snowwalker
 * @date 2024/4/1
 */
class MimeTypeEnumTest {

    @Test
    void getMimeTypeFromFilename() {
        String filename = "操作手册.DOC";
        String doc = MimeTypeEnum.getMimeTypeByFilename(filename);
        assertEquals(MimeTypeEnum.DOC.getMimeType(), doc);
    }

    @Test
    void getMimeType() {
        String extension = ".DOC";
        String doc = MimeTypeEnum.getMimeType(extension);
        assertEquals(MimeTypeEnum.DOC.getMimeType(), doc);
    }

    @Test
    void getMimeTypeEnum() {
        String extension = ".DOC";
        MimeTypeEnum mimeTypeEnum = MimeTypeEnum.getMimeTypeEnum(extension);
        assertEquals(MimeTypeEnum.DOC, mimeTypeEnum);
    }
}
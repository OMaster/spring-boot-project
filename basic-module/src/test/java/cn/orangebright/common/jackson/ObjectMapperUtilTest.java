package cn.orangebright.common.jackson;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import cn.orangebright.common.response.PageResult;
import cn.orangebright.common.util.JsonUtil;
import cn.orangebright.common.util.jackson.ObjectMapperUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author orangebright
 * @date 2023/4/20
 */
class ObjectMapperUtilTest {

	PageResult pageEntity = new PageResult();

	@BeforeEach
	void setUp() {
		pageEntity.setPageIndex(1);
		pageEntity.setPageSize(100);
		pageEntity.setDataList(null);
	}

	@Test
	void getInstance() throws Exception {
		// 携带全限定类名的测试
		ObjectMapper objectMapper = ObjectMapperUtil.create();
		ObjectMapperUtil.addClassName(objectMapper);

		// 这里已经进行序列化操作，后面的excludeNullField配置不生效
		String s = objectMapper.writeValueAsString(pageEntity);
		System.out.println(s);
		System.out.println(objectMapper.readValue(s, PageResult.class));

		ObjectMapperUtil.excludeNullField(objectMapper);
		// excludeNullField 配置不生效，因为配置前已使用 objectMapper 进行序列化操作
		System.out.println("未成功排除掉null字段：" + objectMapper.writeValueAsString(pageEntity));

		ObjectMapper objectMapper1 = ObjectMapperUtil.create();
		ObjectMapperUtil.addClassName(objectMapper1);
		ObjectMapperUtil.excludeNullField(objectMapper1);
		// excludeNullField 配置生效
		System.out.println("成功排除掉null字段：" + objectMapper1.writeValueAsString(pageEntity));
	}

	@Test
	void create() {
		System.out.println(JsonUtil.objectToString(pageEntity));

		// 对象序列化结果携带全限定类名的测试
		ObjectMapper objectMapper = ObjectMapperUtil.create();
		ObjectMapperUtil.addClassName(objectMapper);
		try {
			String s = objectMapper.writeValueAsString(pageEntity);
			System.out.println(s);
			System.out.println(objectMapper.readValue(s, PageResult.class));
		}
		catch (JsonProcessingException e) {

		}

		System.out.println(JsonUtil.objectToString(pageEntity));
	}

	@Test
	void javaTimeTest() throws JsonProcessingException {
		ObjectMapper objectMapper = ObjectMapperUtil.create();

//		System.out.println(JsonUtil.objectToString(new A("a1", LocalDateTime.now())));
		System.out.println(objectMapper.writeValueAsString(new B("b1", LocalDateTime.now())));

		JavaTimeModule javaTimeModule = new JavaTimeModule();
		objectMapper.registerModule(javaTimeModule);
		System.out.println(objectMapper.writeValueAsString(new A("a2", LocalDateTime.now())));
		System.out.println(objectMapper.writeValueAsString(new B("b2", LocalDateTime.now())));

		String a3 = objectMapper.writeValueAsString(new A("a3", LocalDateTime.now()));
		String b3 = objectMapper.writeValueAsString(new B("b3", LocalDateTime.now()));
		System.out.println(a3);
		System.out.println(b3);

		System.out.println(objectMapper.readValue(a3, A.class));
		System.out.println(objectMapper.readValue(b3, B.class));


		System.out.println();
		ObjectMapper objectMapper2 = ObjectMapperUtil.create();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dtf));
		javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dtf));
		objectMapper2.registerModule(javaTimeModule);

		String a4 = objectMapper2.writeValueAsString(new A("a4", LocalDateTime.now()));
		String b4 = objectMapper2.writeValueAsString(new B("b4", LocalDateTime.now()));
		System.out.println(a4);
		System.out.println(b4);

		System.out.println(objectMapper2.readValue(a4, A.class));
		System.out.println(objectMapper2.readValue(b4, B.class));
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class A {
		String name;

		LocalDateTime ldt;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class B {
		String name;

		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@JsonSerialize(using = LocalDateTimeSerializer.class)
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		LocalDateTime ldt;
	}
}
package cn.orangebright.common.request.xss;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Safelist;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author snowwalker
 * @date 2024/3/5
 */
class JsoupUtilTest {

    @Test
    void clean() {
        String unsafeInput = "<p><a href='http://example.com/' onclick='stealCookies()'>Link</a></p>";
        System.out.println(unsafeInput);
        String safeOutput = Jsoup.clean(unsafeInput, "", Safelist.relaxed(), new Document.OutputSettings().syntax(Document.OutputSettings.Syntax.html));
        System.out.println(safeOutput);

        unsafeInput = "asd<script>";
        System.out.println(unsafeInput);
        safeOutput = Jsoup.clean(unsafeInput, Safelist.relaxed());
        System.out.println(safeOutput);
    }
}
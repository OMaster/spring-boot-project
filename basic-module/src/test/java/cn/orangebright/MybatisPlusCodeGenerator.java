package cn.orangebright;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cn.orangebright.common.bean.BaseEntity;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * Mybatis-plus 代码生成器配置
 *
 * @author orangebright
 * @date 2023/4/13
 */
public class MybatisPlusCodeGenerator {

    /**
     * 项目绝对路径
     */
    public static final String BASIC_PATH = "D:\\Code\\Git\\RuoYi\\RuoYi-study";

    /**
     * 生成类的包名
     */
    public static final String BASIC_PACKAGE = "cn.snowwalker.ruoyistudy";

    /**
     * 包名下的模块名称
     */
    public static final String MODULE_NAME = "system";

    /**
     * 生成类的作者
     */
    public static final String AUTHOR = "snowwalker";

    /*********************************************************************************
     *                      数据库连接信息
     *********************************************************************************/
    private static final String DB_NAME = "ruoyi";

    public static final String USERNAME = "root";

    public static final String PASSWORD = "123456";

    public static final String URL = "jdbc:mysql://127.0.0.1:3306/" + DB_NAME +
            "?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai";


    public static void main(String[] args) {
        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                .globalConfig(builder -> {
                    builder.author(AUTHOR) // 设置作者
//                            .fileOverride() // 覆盖已生成文件
                            .outputDir(BASIC_PATH + "\\src\\main\\java"); // 指定java输出目录
                })
                .packageConfig(builder -> {
                    builder.parent(BASIC_PACKAGE) // 设置父包名
                            .moduleName(MODULE_NAME) // 设置父包模块名
                            .entity("entity")         // 设置实体包名，默认：entity
                            .pathInfo(Collections.singletonMap(OutputFile.xml, BASIC_PATH + "\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig((scanner, builder) ->
                        builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入 all")))
                                .entityBuilder()
                                .superClass(BaseEntity.class)    // 设置父类
                                .disableSerialVersionUID()      // 禁用生成 serialVersionUID
                                .enableChainModel()             // 开启lombok链式调用
                                .enableLombok()                 // 启用lombok注解
                                .enableTableFieldAnnotation()   // 生成字段注解
                                .controllerBuilder()
                                .enableRestStyle()              // 生成@RestController 控制器
                                .enableHyphenStyle()            // 开启驼峰转连字符
                                .serviceBuilder().formatServiceFileName("%sService")
                )
                .templateConfig(builder -> builder.entity("/templates/my-entity.java"))        // 使用自定义模板，将get/set注解换位@Data
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

    // 处理 all 情况
    protected static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }
}

package cn.orangebright.vmdemo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author snowwalker
 * @date 2023/4/10
 */
public class OOMObject {

    public byte[] placeholder = new byte[64 * 1024];

    public static void fillHeap(int num) throws InterruptedException {
        List<OOMObject> list = new ArrayList<OOMObject>();
        for (int i = 0; i < num; i++) {
            Thread.sleep(50);
            list.add(new OOMObject());
        }
        System.gc();
    }

    public static void main(String[] args) throws Exception {
        String s = "hell.zip";

        System.out.println(s.endsWith(".zip"));
//        fillHeap(1000);
//        System.gc();

//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        br.readLine();
//        createBusyThread();
//        br.readLine();
//        Object obj = new Object();
//        createLockThread(obj);
    }

    public static void createBusyThread() {
        Thread thread = new Thread(() -> {
            while (true);
        }, "testBusyThread");
        thread.start();
    }

    public static void createLockThread(final Object lock) {
        Thread thread = new Thread(() -> {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }, "testLockThread");
        thread.start();
    }
}

1、配置文件和格式

- JDK SPI：使用 META-INF/services/ 目录下的文本文件，内容为实现类全限定名列表。
- Dubbo SPI：使用三个目录下的配置文件，内容为 KV 形式的配置，支持别名和默认实现指定。三个目录优先级如下所示：
    - META-INF/dubbo/internal/
    - META-INF/dubbo/
    - META-INF/services/

2、初始化时机

- JDK SPI：JDK SPI 会一次性加载并实例化所有服务实现，可能导致不必要的资源消耗和初始化问题（如初始化耗时长但未被立即使用的实现）。
- Dubbo SPI：Dubbo SPI 支持懒加载，只有在真正需要时才会实例化对应的实现类，提高了资源利用率和启动速度。
package cn.orangebright.spi;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public interface SpiService {

    void say(String words);
}

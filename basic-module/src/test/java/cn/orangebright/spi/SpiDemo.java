package cn.orangebright.spi;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public class SpiDemo {

    public static void main(String[] args) {
        ServiceLoader<SpiService> load = ServiceLoader.load(SpiService.class);

        // for each 调用
        load.forEach(data -> data.say("one"));

        System.out.println();

        // iterator 调用
        Iterator<SpiService> iterator = load.iterator();
        while (iterator.hasNext()) {
            SpiService spiService = iterator.next();
            spiService.say("the first spi demo");
        }
    }
}

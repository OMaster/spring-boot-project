package cn.orangebright.spi;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public class SpiServiceImpl implements SpiService{
    @Override
    public void say(String words) {
        System.out.println("hello snowwalker:" + words);
    }
}

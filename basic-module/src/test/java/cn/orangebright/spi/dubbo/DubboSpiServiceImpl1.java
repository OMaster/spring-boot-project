package cn.orangebright.spi.dubbo;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public class DubboSpiServiceImpl1 implements DubboSpiService {
    @Override
    public void say(String words) {
        System.out.println("The first dubbo spi impl :" + words);
    }
}

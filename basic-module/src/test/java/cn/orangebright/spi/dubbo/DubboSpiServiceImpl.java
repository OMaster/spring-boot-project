package cn.orangebright.spi.dubbo;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public class DubboSpiServiceImpl implements DubboSpiService {
    @Override
    public void say(String words) {
        System.out.println("The default dubbo spi impl :" + words);
    }
}

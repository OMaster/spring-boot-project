package cn.orangebright.spi.dubbo;

import org.apache.dubbo.common.extension.SPI;

/**
 * @author orangebright
 * @date 2023/3/23
 */
@SPI("two")
public class DubboSpiServiceImpl2 implements DubboSpiService {
    @Override
    public void say(String words) {
        System.out.println("DubboSpiServiceImpl2");
    }
}

package cn.orangebright.spi.dubbo;

import org.apache.dubbo.common.extension.ExtensionLoader;

/**
 * @author orangebright
 * @date 2023/3/23
 */
public class DubboSpiDemo {

    public static void main(String[] args) {
        // 1、获取指定 spi 接口的 ExtensionLoader 对象
        ExtensionLoader<DubboSpiService> load = ExtensionLoader.getExtensionLoader(DubboSpiService.class);

        // 2、获取默认的扩展实现类对象（即 Spi 注解的value属性值对应的实现类），如果Spi注解未指定value属性，则返回null
        DubboSpiService defaultExtension = load.getDefaultExtension();
        defaultExtension.say("hello dubbo spi");

        // 3、获取指定名称的扩展实现类对象
        DubboSpiService two = load.getExtension("two");
        two.say("hello dubbo spi");

        DubboSpiService two2 = load.getExtension("two");

        // 多个扩展名对应同一个对象
        DubboSpiService one = load.getExtension("one");
        DubboSpiService first = load.getExtension("first");
        System.out.println(one.equals(first));
        first.say("hello dubbo spi");

    }
}

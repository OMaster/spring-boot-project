package cn.orangebright.spi.dubbo;

import org.apache.dubbo.common.extension.SPI;

/**
 * @author orangebright
 * @date 2023/3/23
 */
@SPI("default")
public interface DubboSpiService {

    void say(String words);
}

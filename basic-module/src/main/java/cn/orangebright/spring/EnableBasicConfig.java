package cn.orangebright.spring;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * 启用基础模块bean功能
 *
 * @author orangebright
 * @date 2022/12/21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(BasicModuleConfiguration.class)
public @interface EnableBasicConfig {
}

package cn.orangebright.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 基础模块配置中心，配置所有bean，通过{@link EnableBasicConfig}启用
 *
 * @author orangebright
 * @date 2022/12/21
 */
@Configuration
@ComponentScan
public class BasicModuleConfiguration {
}

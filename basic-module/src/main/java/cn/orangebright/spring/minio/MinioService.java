package cn.orangebright.spring.minio;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.Resource;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.ListObjectsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveBucketArgs;
import io.minio.RemoveObjectArgs;
import io.minio.Result;
import io.minio.http.Method;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * minio接口
 * @author snowwalker
 * @date 2023/12/9
 */
@Service
@ConditionalOnClass(MinioClient.class)
public class MinioService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinioService.class);

    @Resource
    private MinioProperties minioProperties;
    @Resource
    private MinioClient minioClient;

    /**
     * 判断bucket是否存在
     * @param bucketName 存储桶名称
     * @return
     */
    public boolean hasBucket(String bucketName) {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            LOGGER.error("判断bucket是否存在异常", e);
            return false;
        }
    }


    /**
     * 创建存储桶
     * @param bucketName 存储桶名称
     */
    @SneakyThrows(Exception.class)
    public void createBucket(String bucketName) {
        if (!hasBucket(bucketName)) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }
    }

    /**
     * 删除存储桶
     * @param bucketName 存储桶名称
     */
    @SneakyThrows(Exception.class)
    public void deleteBucket(String bucketName) {
        boolean hasBucket = this.hasBucket(bucketName);
        if (hasBucket) {
            minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
        }
    }

    /**
     * 获取存储桶列表
     * @return
     */
    @SneakyThrows(Exception.class)
    public List<Bucket> getBuckets() {
        // Bucket 对象中属性的get/set方法不是标准实现，故无法序列化
        return minioClient.listBuckets();
    }


    /**
     * 获取文件列表
     * @return
     */
    @SneakyThrows
    public List<Item> listObjects() {
        Iterable<Result<Item>> results = minioClient.listObjects(
                ListObjectsArgs.builder().bucket(minioProperties.getBucketName()).build());
        List<Item> items = new ArrayList<>();
        for (Result<Item> result : results) {
            items.add(result.get());
        }
        // Item 对象的get/set方法不是标准实现，字段不能序列化
        return items;
    }

    /**
     * 上传文件
     * @param bucketName  存储桶名称
     * @param fileName    文件名称
     * @param contentType 文件类型
     * @param stream      文件流
     * @return
     * @throws Exception
     */
    public String uploadFile(String bucketName, String fileName, String contentType, InputStream stream) throws Exception {
        checkBucket(bucketName);
        minioClient.putObject(PutObjectArgs.builder()
                .bucket(bucketName)
                .object(fileName)
                .stream(stream, stream.available(), -1)
                .contentType(contentType)
                .build());
        return getFileUrl(bucketName, fileName);
    }

    /**
     * 获取文件访问地址
     * @param bucketName 存储桶名称
     * @param fileName   文件名称
     * @return
     */
    public String getFileUrl(String bucketName, String fileName) throws Exception {
        checkBucket(bucketName);
        GetPresignedObjectUrlArgs build = GetPresignedObjectUrlArgs.builder()
                .bucket(bucketName)
                .object(fileName)
                .method(Method.GET)
                .build();
        return minioClient.getPresignedObjectUrl(build);
    }

    /**
     * 下载文件
     * @param bucketName    存储桶名称
     * @param fileName  文件名
     * @return
     * @throws Exception
     */
    public InputStream downloadFile(String bucketName, String fileName) throws Exception {
        checkBucket(bucketName);
        return minioClient.getObject(GetObjectArgs.builder()
                .bucket(bucketName)
                .object(fileName)
                .build());
    }

    /**
     * 删除文件
     * @param bucketName 存储桶
     * @param fileName 文件名称
     */
    public void deleteFile(String bucketName, String fileName) throws Exception {
        checkBucket(bucketName);
        minioClient.removeObject(RemoveObjectArgs.builder().
                bucket(bucketName)
                .object(fileName)
                .build());
    }

    private void checkBucket(String bucketName) {
        if (!hasBucket(bucketName)) {
            throw new RuntimeException("存储桶不存在");
        }
        Assert.isTrue(hasBucket(bucketName), "存储桶不存在");
    }


}

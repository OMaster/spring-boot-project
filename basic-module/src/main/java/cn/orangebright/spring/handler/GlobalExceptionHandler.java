package cn.orangebright.spring.handler;

import java.util.List;
import java.util.StringJoiner;

import cn.orangebright.common.exception.ServiceException;
import cn.orangebright.common.response.ResultBack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 * @author orangebright
 * @date 2021/12/1
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 自定义异常类处理逻辑
     * @param ex 自定义异常对象{@link ServiceException}
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    public ResultBack<?> handleServiceException(ServiceException ex) {
        LOGGER.error("业务异常：{}", ex.getMessage());
        return ResultBack.failure(ex.getMessage());
    }

    /**
     * 控制层参数校验异常处理逻辑
     * @param ex 参数校验异常对象
     * @return
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public ResultBack<?> handleParamValidateException(BindException ex) {
        LOGGER.error("参数绑定异常:", ex);
        StringJoiner errorMsg = new StringJoiner("；");
        // 获取所有字段验证出错的信息
        List<FieldError> allErrors = ex.getFieldErrors();
        allErrors.forEach(fieldError -> errorMsg.add(fieldError.getDefaultMessage()));
        return ResultBack.failure(errorMsg.toString());
    }

    /**
     * 处理 SpringMVC 请求方法不正确
     * <p>
     * 例如说，A 接口的方法为 GET 方式，结果请求方法为 POST 方式，导致不匹配
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResultBack<?> httpRequestMethodNotSupportedExceptionHandler(HttpRequestMethodNotSupportedException ex) {
        LOGGER.error("[httpRequestMethodNotSupportedExceptionHandler]", ex);
        return ResultBack.failure(String.format("请求方法不正确:%s", ex.getMessage()));
    }

    /**
     * 全局异常对 Exception 的处理，返回：服务异常
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResultBack<?> handleException(Exception ex) {
        LOGGER.error("全局异常捕获：", ex);
        return ResultBack.serverError();
    }

}

package cn.orangebright.spring.handler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 全局统一返回对象标识注解，控制层类上使用该注解后统一返回ResultBack类型
 * @author snowwalker
 * @date 2024/2/19
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GlobalResponse {
}

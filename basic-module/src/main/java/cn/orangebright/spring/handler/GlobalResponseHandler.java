package cn.orangebright.spring.handler;

import cn.orangebright.common.response.ResultBack;
import cn.orangebright.common.util.JsonUtil;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 全局接口返回值封装处理类
 *
 * @author orangebright
 * @date 2021/12/1
 */
@RestControllerAdvice(annotations = GlobalResponse.class)
public class GlobalResponseHandler implements ResponseBodyAdvice<Object> {
    /**
     * 接口返回对象为ResultBack类型时，返回false，否则返回true
     * @param returnType
     * @param converterType
     * @return
     */
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        // 这里的参数类型对应控制层方法的返回值类型，详见ReturnValueMethodParameter
        return returnType.getParameterType() != ResultBack.class;
    }

    /**
     * 将非ResultBack类型对象封装为ResultBack对象返回，String类型需特殊处理
     *  supports()方法为true时，才执行此方法
     *  执行时机：在将接口返回对象写入response之前（beforeBodyWrite）
     * @param body
     * @param returnType
     * @param selectedContentType
     * @param selectedConverterType
     * @param request
     * @param response
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        /**
         * 接口返回类型为String需特殊处理的原因：
         *  在所有的 HttpMessageConverter 实例集合中，StringHttpMessageConverter 转换器优先级较高，类型相同，且支持解析所有MediaType类型，
         *  故返回String类型时会使用StringHttpMessageConverter来处理返回的值，
         *  如果返回的是对象，因为类型不匹配，故跳过StringHttpMessageConverter，直到MappingJackson2HttpMessageConverter转换器匹配成功，将对象处理为json串
         *
         * 默认优先级见{@link org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver#messageConverters}的列表元素顺序
         * 实际调用判断方法见{@link org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodProcessor#writeWithMessageConverters(Object, MethodParameter, ServletServerHttpRequest, ServletServerHttpResponse)}方法
         *
         * 解决方案：
         *  1、将String类型单独处理，将封装的返回对象手动转换为String类型的json串
         *  2、将Object类型的转换器 MappingJackson2HttpMessageConverter 的顺序提到 StringHttpMessageConverter 前面，
         *      然后再返回String类型的方法注解RequestMapping中通过produces属性指明返回数据的MediaType类型为：application/json。
         *      因为返回String类型的方法请求默认的MediaType类型为：text/html，和MappingJackson2HttpMessageConverter支持的类型不一致
         *
         *  综上所述，推荐使用第一种方式，即如下所示，手动处理，保留默认的mvc配置
         *      第二种方式不仅需要额外配置，还需要在每个返回String类型的方法上指定produces属性值，详见{@link cn.orangebright.spring.config.WebConfig}
         */
        if (returnType.getParameterType().equals(String.class)) {
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            return JsonUtil.objectToString(ResultBack.success(body));
        }
        return ResultBack.success(body);
    }
}

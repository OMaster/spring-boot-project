package cn.orangebright.spring.config;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.boot.autoconfigure.task.TaskSchedulingAutoConfiguration;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AnnotationAsyncExecutionInterceptor;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 调度器配置
 * 只单独配置调度器，会使springboot自动配置的任务执行器失效，详见{@link TaskExecutionAutoConfiguration#applicationTaskExecutor(TaskExecutorBuilder)}
 * <p>如果想要自己配置，两种方式：1、通过属性修改默认值；2、同时自定义调度器和任务执行器
 *
 * @author orangebright
 * @date 2023/4/23
 */
@Configuration(value = "cn.orangebright.spring.config.ScheduleConfig", proxyBeanMethods = false)
public class ScheduleConfig {

    /**
     * 核心线程数
     */
    private int corePoolSize = 10;

    /**
     * 最大线程数
     */
    private int maxPoolSize = 100;

    /**
     * 队列容量
     */
    private int queueCapacity = 1000;

    /**
     * 空闲线程存活时间：5 分钟
     */
    private int keepAliveSeconds = 5 * 60;

    private static final String scheduledExecutorThreadNamePrefix = "schedule-pool-";

    public static final String taskExecutorThreadNamePrefix = "task-pool-";

    /**
     * 任务调度器的线程池，替换默认的单线程调度器
     * <p>如果有多个地方配置了 ScheduledExecutorService 类型的bean，则bean名称为 taskScheduler 的配置生效
     * <p>spring boot 中可以通过 spring.task.scheduling.pool.size 属性调整调度器，详见{@link TaskSchedulingAutoConfiguration}
     * <p>spring boot 中该bean会导致 ThreadPoolTaskExecutor 实例化失败，详见{@link TaskExecutionAutoConfiguration#applicationTaskExecutor(TaskExecutorBuilder)}
     *
     * @see ScheduledAnnotationBeanPostProcessor#finishRegistration()
     * @see ScheduledTaskRegistrar#afterPropertiesSet()
     */
    @Bean
    @ConditionalOnMissingBean(name = "taskScheduler")
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler executor = new ThreadPoolTaskScheduler();
        executor.setPoolSize(corePoolSize);
        executor.setThreadNamePrefix(taskExecutorThreadNamePrefix);
        return executor;
    }

    /**
     * 任务异步执行器的线程池，名称为taskExecutor的优先级最高
     * @return
     */
    @Bean(name = AnnotationAsyncExecutionInterceptor.DEFAULT_TASK_EXECUTOR_BEAN_NAME)
    @ConditionalOnMissingBean(name = AnnotationAsyncExecutionInterceptor.DEFAULT_TASK_EXECUTOR_BEAN_NAME)
    public ThreadPoolTaskExecutor taskExecutor()
    {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(maxPoolSize);
        executor.setCorePoolSize(corePoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(keepAliveSeconds);
        executor.setThreadNamePrefix(taskExecutorThreadNamePrefix);
        // 线程池对拒绝任务(无线程可用)的处理策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }
}

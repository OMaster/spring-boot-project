package cn.orangebright.spring.config;

import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * druid常用配置
 *
 * @author orangebright
 * @date 2023/4/21
 */
@Configuration(value = "cn.orangebright.spring.config.druidConfig", proxyBeanMethods = false)
@ConditionalOnClass({WallFilter.class, WallConfig.class})
public class DruidConfig {

    @Bean
    @ConditionalOnMissingBean
    public WallFilter wallFilter(WallConfig wallConfig) {
        WallFilter wallFilter = new WallFilter();
        wallFilter.setConfig(wallConfig);
        return wallFilter;
    }

    @Bean
    @ConditionalOnMissingBean
    public WallConfig wallConfig() {
        WallConfig wallConfig = new WallConfig();
        // 不允许使用 select *
        wallConfig.setSelectAllColumnAllow(false);
        // 允许update 和 delete 操作没有 where 条件
        wallConfig.setUpdateWhereNoneCheck(true);
        wallConfig.setDeleteWhereNoneCheck(true);

        // 不允许使用 truncate、create、alter、drop table
        wallConfig.setTruncateAllow(false);
        wallConfig.setCreateTableAllow(false);
        wallConfig.setAlterTableAllow(false);
        wallConfig.setDropTableAllow(false);

        return wallConfig;
    }
}

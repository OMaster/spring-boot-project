package cn.orangebright.spring.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate配置类
 *
 * @author orangebright
 * @date 2022/1/8
 */
@Configuration(value = "cn.orangebright.spring.config.RestTemplateConfig", proxyBeanMethods = false)
public class RestTemplateConfig {

    /**
     * 通过RestTemplateBuilder来获取RestTemplate
     * <p>
     * 其内部会有两个StringHttpMessageConverter，一个是UTF-8编码，一个是ISO-8859-1
     *
     * @param restTemplateBuilder
     * @return
     */
    @Bean
    @Lazy
    @ConditionalOnMissingBean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }
}

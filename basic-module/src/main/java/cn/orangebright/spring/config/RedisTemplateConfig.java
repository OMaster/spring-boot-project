package cn.orangebright.spring.config;

import cn.orangebright.common.util.jackson.ObjectMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * RedisTemplate配置类
 *
 * @author orangebright
 * @date 2022/1/8
 */
@Configuration(value = "cn.orangebright.spring.config.RedisTemplateConfig", proxyBeanMethods = false)
@ConditionalOnClass(RedisOperations.class)
public class RedisTemplateConfig {

    @Bean
    @ConditionalOnMissingBean(name = "redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        // 1. 将RedisTemplate<Object, Object>修改为RedisTemplate<String, Object>类型
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 2. 连接工厂
        template.setConnectionFactory(redisConnectionFactory);

        // 3. 配置具体的序列化方式
        // 3.1 String 的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // 3.2 jackson的序列化配置
        ObjectMapper objectMapper = ObjectMapperUtil.create();
        ObjectMapperUtil.addClassName(objectMapper);
        GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer(objectMapper);

        // 4. 指定redis的key和value的序列化方式，避免使用默认的JDK序列化
        // key采用String类型序列化
        template.setKeySerializer(stringRedisSerializer);
        // hash类型的key也采用String类型序列化
        template.setHashKeySerializer(stringRedisSerializer);
        // value采用jackson方式序列化
        template.setValueSerializer(jsonRedisSerializer);
        // hash类型的value采用jackson方式序列化
        template.setHashValueSerializer(jsonRedisSerializer);

        return template;
    }


    /**
     * 修改redis配置信息（主机和端口，也可以是其它信息）
     * @param applicationContext
     * @param rsc
     */
    public void changeRedisConfig(ApplicationContext applicationContext, RedisStandaloneConfiguration rsc) {
        RedisConnectionFactory redisConnectionFactory = applicationContext.getBean(RedisConnectionFactory.class);
        if (redisConnectionFactory instanceof JedisConnectionFactory jedisConnectionFactory) {
            changeJedisConnectionFactory(jedisConnectionFactory, rsc.getHostName(), jedisConnectionFactory.getPort());
        } else if (redisConnectionFactory instanceof LettuceConnectionFactory) {
            changeLettuceConnectionFactory((LettuceConnectionFactory) redisConnectionFactory, rsc);
        }
    }

    /**
     * 2.x.x版本实现
     * @param jedisConnectionFactory
     * @param ip
     * @param port
     */
    public void changeJedisConnectionFactory(JedisConnectionFactory jedisConnectionFactory, String ip, int port) {
        // 1、更新主机和端口
        RedisStandaloneConfiguration standaloneConfiguration = jedisConnectionFactory.getStandaloneConfiguration();
        standaloneConfiguration.setHostName(ip);
        standaloneConfiguration.setPort(port);

        // 2、清空旧的连接池，不用setShardInfo(null)，因为新版本获取连接时不是通过ShardInfo来获取ip和端口，即使ShardInfo为旧的也不影响
        // 详见JedisConnectionFactory.createJedis
        jedisConnectionFactory.destroy();

        // 3、初始化连接工厂（主要为连接池）
        jedisConnectionFactory.afterPropertiesSet();
    }

    /**
     * 低版本(1.x.x)实现
     * @param jedisConnectionFactory
     * @param ip
     * @param port
     */
    private void one(JedisConnectionFactory jedisConnectionFactory, String ip, int port) {
//        // 1、更新主机和端口
//        jedisConnectionFactory.setHostName(ip);
//        jedisConnectionFactory.setPort(port);
//        // 2、清空旧的连接池和对象
//        jedisConnectionFactory.destroy();
//        jedisConnectionFactory.setShardInfo(null);
//        // 3、初始化连接工厂
//        jedisConnectionFactory.afterPropertiesSet();
    }

    /**
     * 修改LettuceConnectionFactory的redis配置
     * @param lettuceConnectionFactory
     * @param redisStandaloneConfiguration
     */
    private void changeLettuceConnectionFactory(LettuceConnectionFactory lettuceConnectionFactory, RedisStandaloneConfiguration redisStandaloneConfiguration) {
        RedisStandaloneConfiguration standaloneConfig = lettuceConnectionFactory.getStandaloneConfiguration();
        // 1、更新主机和端口
        standaloneConfig.setHostName(redisStandaloneConfiguration.getHostName());
        standaloneConfig.setPort(redisStandaloneConfiguration.getPort());
        // 2、初始化连接工厂
        lettuceConnectionFactory.afterPropertiesSet();
    }
}

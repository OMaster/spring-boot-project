package cn.orangebright.spring.config;

import java.util.List;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * GlobalResponseHandler类中对String类型返回值的特殊处理方式二：
 * <p>
 *  2.1 使用 @Configuration 注解 + extendMessageConverters方法<br/>
 *      保留Spring Boot关于mvc的自动配置，但是需要在每个返回String类型的方法上指定RequestMapping注解的produces属性值为：application/json
 * </p>
 *
 * <p>
 *  2.2 使用 @Configuration 注解 、@EnableWebMvc注解 + configureMessageConverters方法<br/>
 *      该方式会覆盖掉Spring Boot关于mvc的自动配置，但无需指定produces属性值
 * </p>
 *
 * <p></p>
 * 无论使用那种配置，都建议使用{@link cn.orangebright.spring.handler.GlobalResponseHandler}中的处理方式一，比较简单
 *
 * @author orangebright
 * @date 2021/12/1
 */
//@EnableWebMvc
//@Configuration
@Deprecated
public class WebConfig implements WebMvcConfigurer {

//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.removeIf(converter -> converter instanceof MappingJackson2HttpMessageConverter);
//        converters.add(0, new MappingJackson2HttpMessageConverter());
//    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 提高 MappingJackson2HttpMessageConverter 的优先级：先移除再添加到第一个元素
        converters.removeIf(converter -> converter instanceof MappingJackson2HttpMessageConverter);
        converters.add(0, new MappingJackson2HttpMessageConverter());
    }
}
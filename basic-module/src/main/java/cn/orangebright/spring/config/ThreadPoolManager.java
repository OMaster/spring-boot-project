package cn.orangebright.spring.config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import cn.orangebright.common.threadpool.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

/**
 * 线程池管理器
 *
 * @author orangebright
 * @date 2023/8/15
 */
@Component
public class ThreadPoolManager implements DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolManager.class);

    private static List<ExecutorService> executorServices = new ArrayList<>();

    /**
     * 添加线程池，保证线程池的关闭
     * @param executorService
     */
    public static void addExecutorService(ExecutorService executorService) {
        executorServices.add(executorService);
    }

    @Override
    public void destroy() throws Exception {
        logger.info("====关闭后台任务线程池====");
        executorServices.forEach(ThreadPoolUtil::shutdown);
    }

}

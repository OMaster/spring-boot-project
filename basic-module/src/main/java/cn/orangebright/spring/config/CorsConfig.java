package cn.orangebright.spring.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 跨域请求配置类
 *
 * @author orangebright
 * @date 2022/6/11
 */
@Configuration(value = "cn.orangebright.spring.config.CorsConfig", proxyBeanMethods = false)
public class CorsConfig {

    @Bean
    @ConditionalOnMissingBean
    public CorsFilter corsFilter() {
        // 1、配置Cors 配置信息
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 放行那些原始请求域
        corsConfiguration.addAllowedOriginPattern("*");
        // 放行哪些原始请求头部信息
        corsConfiguration.addAllowedHeader("*");
        // 放行哪些请求方式
        corsConfiguration.addAllowedMethod("*");
        // 是否发送Cookie
        corsConfiguration.setAllowCredentials(true);
        // 2、添加映射路径
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        // 3、返回新的CorsFilter
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}

package cn.orangebright.spring;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * 将日期格式【yyyy-MM-dd】、日期时间格式【yyyy-MM-dd HH:mm:ss】的字符串转换为LocalDateTime对象
 * @author snowwalker
 * @date 2023/9/19
 */
@Component
public class StringToLocalDateTimeConvert implements Converter<String, LocalDateTime> {

    /**
     * 时间标识
     */
    private static final String TIME_MARK = ":";

    /**
     * 日期时间格式
     */
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 格式化对象
     */
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

    @Override
    public LocalDateTime convert(String source) {
        // 是日期时间格式，直接解析
        if (source.contains(TIME_MARK)) {
            return LocalDateTime.parse(source, DATE_TIME_FORMATTER);
        }
        // 日期格式，先解析为日期，然后拼接时间
        LocalDate localDate = LocalDate.parse(source);
        return LocalDateTime.of(localDate, LocalTime.MIN);
    }

}

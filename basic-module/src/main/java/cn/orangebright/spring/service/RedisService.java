package cn.orangebright.spring.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import jakarta.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;


/**
 * Redis相关操作工具类
 * <p>
 * 用StringRedisSerializer进行序列化的值，在Java和Redis中保存的内容是一样的
 * 用Jackson2JsonRedisSerializer进行序列化的值，在Redis中保存的内容，比Java中多了一对双引号。
 *
 * @author orangebright
 * @date 2021/6/9
 */
@Component
@ConditionalOnClass(RedisOperations.class)
public class RedisService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisService.class);

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 默认过期时间：1小时
     */
    private static final Long DEFAULT_TIMEOUT = 60 * 60L;

    /*********************************************************************
     *                      Keys
     *********************************************************************/

    /**
     * 判断key是否存在
     *
     * @param key
     * @return
     */
    public boolean hasKey(String key) {
        return key != null && Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }

    /**
     * 删除单个key
     *
     * @param key
     * @return
     */
    public boolean delKey(String key) {
        return key != null && Boolean.TRUE.equals(redisTemplate.delete(key));
    }

    /**
     * 删除多个key
     *
     * @param key
     * @return
     */
    public long delKey(String... key) {
        if (key == null || key.length == 0) {
            return 0L;
        }
        Long result = redisTemplate.delete(Arrays.asList(key));
        return result == null ? 0L : result;
    }

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间，单位：秒
     * @return
     */
    public boolean expire(String key, long time) {
        if (time <= 0) {
            return false;
        }
        return expire(key, time, TimeUnit.SECONDS);
    }

    public boolean expire(String key, long time, TimeUnit timeUnit) {
        if (key == null || timeUnit == null) {
            return false;
        }
        return Boolean.TRUE.equals(redisTemplate.expire(key, time, timeUnit));
    }

    /**
     * 获取指定key的过期时间
     *
     * @param key key不存在返回-2，无过期时间返回-1
     * @return 剩余过期时间，单位：秒
     */
    public long getExpire(String key) {
        return getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 获取指定key的过期时间
     *
     * @param key
     * @param timeUnit
     * @return
     */
    public long getExpire(String key, TimeUnit timeUnit) {
        if (key == null) {
            return -2;
        }
        return redisTemplate.getExpire(key, timeUnit);
    }

    /*********************************************************************
     *                      String
     *********************************************************************/

    /**
     * 获取指定key的值
     *
     * @param key 键
     * @return
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 设置永不过期的key
     *
     * @param key 键
     * @param obj
     */
    public boolean setWithNoExpire(String key, Object obj) {
        if (key == null) {
            return false;
        }
        try {
            redisTemplate.opsForValue().set(key, obj);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 设置值并指定过期时间为 1小时
     *
     * @param key 键
     * @param obj 值
     * @return
     */
    public boolean set(String key, Object obj) {
        return this.set(key, obj, DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    /**
     * 设置值并指定过期时间
     *
     * @param key  键
     * @param obj  值
     * @param time 过期时间：秒
     * @return
     */
    public boolean set(String key, Object obj, long time) {
        return this.set(key, obj, time, TimeUnit.SECONDS);
    }

    /**
     * 设置值并指定过期时间
     *
     * @param key      键
     * @param obj      值
     * @param time     过期时间
     * @param timeUnit 过期时间单位
     * @return
     */
    public boolean set(String key, Object obj, long time, TimeUnit timeUnit) {
        if (key == null || timeUnit == null) {
            return false;
        }
        try {
            redisTemplate.opsForValue().set(key, obj, time, timeUnit);
            return true;
        } catch (Exception e) {
            LOGGER.error("设置值并指定过期时间操作异常：", e);
            return false;
        }
    }

    /**
     * 分布式锁
     *
     * @param key 锁
     * @param obj 锁值
     * @return
     */
    public boolean tryLock(String key, Object obj) {
        return this.tryLock(key, obj, DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    /**
     * 分布式锁
     *
     * @param key  锁
     * @param obj  锁值
     * @param time 锁时间
     * @return
     */
    public boolean tryLock(String key, Object obj, long time) {
        return this.tryLock(key, obj, time, TimeUnit.SECONDS);
    }

    /**
     * 分布式锁
     *
     * @param key      锁
     * @param obj      锁值
     * @param time     锁时间
     * @param timeUnit 锁时间单位
     * @return 上锁成功
     */
    public boolean tryLock(String key, Object obj, long time, TimeUnit timeUnit) {
        if (key == null || timeUnit == null) {
            return false;
        }
        return redisTemplate.opsForValue().setIfAbsent(key, obj, time, timeUnit);
    }

    /**
     * 递增加1
     *
     * @param key 键
     * @return
     */
    public long incr(String key) {
        if (key == null) {
            return Long.MIN_VALUE;
        }
        return redisTemplate.opsForValue().increment(key);
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 大于0——则递增；小于0——递减
     * @return
     */
    public long incr(String key, long delta) {
        if (key == null) {
            return Long.MIN_VALUE;
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key 键
     * @return
     */
    public long decr(String key) {
        return redisTemplate.opsForValue().decrement(key);
    }

    // ================================Hash=================================

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return key != null && item != null && redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hDel(String key, Object... item) {
        if (key == null || item == null || item.length == 0) {
            return;
        }
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hGet(String key, String item) {
        return key == null || item == null ? null : redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public List<Object> hmGet(String key, Object... item) {
        return key == null || item == null ? Collections.emptyList() :
                redisTemplate.opsForHash().multiGet(key, Arrays.asList(item));
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key hashKey
     * @return
     */
    public Map<Object, Object> hGetAll(String key) {
        return key == null ? Collections.emptyMap() : redisTemplate.opsForHash().entries(key);
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hSet(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            LOGGER.error("设置hashKey异常，key=" + key + " <" + item + ", " + value + ">：", e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hSet(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                this.expire(key, time);
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("设置hashKey并指定过期时间异常，key=" + key + " <" + item + ", " + value + ">：", e);
            return false;
        }
    }

    /**
     * 设置Hash key
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmSet(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            LOGGER.error("设置设置hashKey异常，key = " + key + "-->" + map + "：", e);
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmSet(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("设置设置hashKey并指定过期时间异常，key = " + key + "-->" + map + "：", e);
            return false;
        }
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hIncr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hDecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    // ============================set=============================

    /**
     * 将数据放入set集合
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sAdd(String key, Object... values) {
        if (key == null || values == null || values.length == 0) {
            return 0L;
        }
        Long count = redisTemplate.opsForSet().add(key, values);
        return count == null ? 0L : count;
    }

    /**
     * 移除集合中指定的值
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long sRem(String key, Object... values) {
        if (key == null || values == null || values.length == 0) {
            return 0L;
        }
        Long count = redisTemplate.opsForSet().remove(key, values);
        return count == null ? 0L : count;
    }

    /**
     * 随机删除指定集合中元素
     *
     * @param key
     * @return
     */
    public Object sPop(String key) {
        return redisTemplate.opsForSet().pop(key);
    }

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sMembers(String key) {
        return key == null ? Collections.emptySet() : redisTemplate.opsForSet().members(key);
    }

    /**
     * 获取set集合大小
     *
     * @param key 键
     * @return
     */
    public long sCard(String key) {
        return key == null ? 0 : redisTemplate.opsForSet().size(key);
    }

    /**
     * 判断value是否存在指定key的set集合中
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sIsMember(String key, Object value) {
        return key != null && Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(key, value));
    }

    // ===============================list=================================

    /**
     * 获取list列表指定范围的值
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<Object> lRange(String key, long start, long end) {
        if (key == null) {
            return Collections.emptyList();
        }
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 获取list列表的长度
     *
     * @param key 键
     * @return
     */
    public long lLen(String key) {
        if (key == null) {
            return 0L;
        }
        Long size = redisTemplate.opsForList().size(key);
        return size == null ? 0L : size;
    }

    /**
     * 将值放入list列表的头部（左边）
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public long lPush(String key, Object value) {
        if (key == null) {
            return 0;
        }
        Long count = redisTemplate.opsForList().leftPush(key, value);
        return count == null ? 0L : count;
    }

    public long lPush(String key, Object... values) {
        if (key == null || values == null || values.length == 0) {
            return 0;
        }
        Long count = redisTemplate.opsForList().leftPushAll(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 将值放入list列表的尾部（右边）
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public long rPush(String key, Object value) {
        if (key == null) {
            return 0;
        }
        Long count = redisTemplate.opsForList().rightPush(key, value);
        return count == null ? 0L : count;
    }


    /**
     * 将值放入list列表的尾部（右边）
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public long rPush(String key, List<Object> value) {
        if (key == null || CollectionUtils.isEmpty(value)) {
            return 0L;
        }
        // TODO: 2022/8/1 集合包含null，返回0，还是筛选出不为null的集合
        long nullNum = value.stream().filter(Objects::isNull).count();
        if (nullNum > 0) {
            return 0L;
        }
        Long count = redisTemplate.opsForList().rightPushAll(key, value);
        return count == null ? 0L : count;
    }

    /**
     * 根据索引修改list中的数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lSet(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            LOGGER.error("根据索引修改列表数据异常：key=" + key + "——index=" + index + "——value=" + value, e);
            return false;
        }
    }

    /**
     * 移除key中的count个value元素
     *
     * @param key   键
     * @param count 移除多少个
     *              大于0 从头到尾匹配删除
     *              小于0 从尾到头匹配删除|count|个
     *              等于0 全部删除
     * @param value 值
     * @return 移除的个数
     */
    public long lRem(String key, long count, Object value) {
        if (key == null) {
            return 0L;
        }
        Long result = redisTemplate.opsForList().remove(key, count, value);
        return result == null ? 0L : result;
    }

    /**
     * 通过索引获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lIndex(String key, long index) {
        if (key == null) {
            return null;
        }
        return redisTemplate.opsForList().index(key, index);
    }
}

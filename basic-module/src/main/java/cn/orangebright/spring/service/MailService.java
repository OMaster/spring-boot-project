package cn.orangebright.spring.service;

/**
 * 邮件服务接口
 *
 * @author snowwalker
 * @date 2023/4/9
 */
public interface MailService {

    /**
     * 发送普通文本内容
     * @param from      邮件发送人
     * @param to        邮件接受人
     * @param subject   邮件主题
     * @param content   邮件内容
     */
    void sendTextMail(String from, String to, String subject, String content);

    /**
     * 发送html内容的邮件
     * @param from      邮件发送人
     * @param to        邮件接受人
     * @param subject   邮件主题
     * @param content   邮件内容
     */
    void sendHtmlMail(String from, String to, String subject, String content);

    /**
     * 发送携带附件的邮件
     * @param from      邮件发送人
     * @param to        邮件接受人
     * @param subject   邮件主题
     * @param content   邮件内容
     * @param filePath  附件目录
     */
    void sendAttachmentMail(String from, String to, String subject, String content, String filePath);
}

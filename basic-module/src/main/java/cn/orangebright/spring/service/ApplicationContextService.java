package cn.orangebright.spring.service;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 获取ApplicationContext对象的静态工具类，必须使用@Component注解，通过构造器方式注入ApplicationContext
 *
 * @author orangebright
 * @date 2021/12/21
 */
@Component
public class ApplicationContextService {

    private static ApplicationContext applicationContext;

    /**
     * 构造器方式注入
     *
     *  静态变量只能由构造器注入或实现ApplicationContextAware接口
     *
     * @param context 容器中的ApplicationContext对象
     */
    public ApplicationContextService(ApplicationContext context) {
        applicationContext = context;
    }

    /**
     * 获取ApplicationContext对象的静态方法
     * @return  ApplicationContext对象
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 获取指定bean名称的对象
     * @param beanName  bean名称
     * @return  bean对象
     */
    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    /**
     * 获取指定名称和类型的bean
     * @param beanName  bean名称
     * @param cla   bean类型
     * @return  bean对象
     */
    public static <T> T getBean(String beanName, Class<T> cla) {
        return applicationContext.getBean(beanName, cla);
    }

    /**
     * 获取指定类型的对象
     * @param cla  类型
     * @return  bean对象
     */
    public static <T> T getBean(Class<T> cla) {
        return applicationContext.getBean(cla);
    }
}

package cn.orangebright.spring.service;

import java.io.File;
import jakarta.activation.MimeType;
import jakarta.annotation.Resource;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import cn.orangebright.common.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 * 邮件服务组件
 *
 * @author orangebright
 * @date 2022/1/9
 */
@Service
@ConditionalOnClass({MimeMessage.class, MimeType.class, MailSender.class})
@ConditionalOnBean(JavaMailSender.class)
public class MailServiceImpl implements MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceImpl.class);

    @Resource
    JavaMailSender mailSender;

    /**
     * 发送普通文本内容
     *
     * @param from    邮件发送人
     * @param to      邮件接受人
     * @param subject 邮件主题
     * @param content 邮件内容
     */
    @Override
    public void sendTextMail(String from, String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        try {
            mailSender.send(message);
            LOGGER.info("邮箱消息发送成功");
        } catch (MailException e) {
            LOGGER.error("邮箱消息发送失败", e);
            throw new ServiceException("邮箱消息发送失败", e);
        }
    }

    /**
     * 发送html内容的邮件
     *
     * @param from    邮件发送人
     * @param to      邮件接受人
     * @param subject 邮件主题
     * @param content 邮件内容
     */
    @Override
    public void sendHtmlMail(String from, String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            // 设置为true表示html内容，否则为普通文本内容
            helper.setText(content, true);

            mailSender.send(message);
            LOGGER.info("邮箱消息发送成功");
        } catch (MessagingException e) {
            LOGGER.error("邮箱消息发送失败", e);
            throw new ServiceException("邮箱消息发送失败", e);
        }
    }

    /**
     * 发送携带附件的邮件
     *
     * @param from     邮件发送人
     * @param to       邮件接受人
     * @param subject  邮件主题
     * @param content  邮件内容
     * @param filePath 附件目录
     */
    @Override
    public void sendAttachmentMail(String from, String to, String subject, String content, String filePath) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content);

            // 读取文件，并上传
            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = file.getFile().getName();
            helper.addAttachment(fileName, file);

            mailSender.send(message);
            LOGGER.info("邮箱消息发送成功");
        } catch (MessagingException e) {
            LOGGER.error("邮箱消息发送失败", e);
            throw new ServiceException("邮箱消息发送失败", e);
        }
    }
}

package cn.orangebright.spring.util;


import jakarta.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 获取ApplicationContext对象的静态工具类
 *
 * @author orangebright
 * @date 2021/12/21
 */
public class ApplicationContextUtil {

    /**
     * 通过 {@link WebApplicationContextUtils} 获取当前 WebApplicationContext，不需要将当前类注入spring容器中
     * @param request http请求
     * @return
     */
    public static WebApplicationContext getWebApplicationContext(HttpServletRequest request) {
        return WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
    }

    /**
     * 获取指定bean名称的对象
     * @param request http请求
     * @param beanName  bean名称
     * @return  bean对象
     */
    public static Object getBean(HttpServletRequest request, String beanName) {
        return getWebApplicationContext(request).getBean(beanName);
    }

    /**
     * 获取指定名称和类型的bean
     * @param request http请求
     * @param beanName  bean名称
     * @param cla   bean类型
     * @return  bean对象
     */
    public static <T> T getBean(HttpServletRequest request, String beanName, Class<T> cla) {
        return getWebApplicationContext(request).getBean(beanName, cla);
    }

    /**
     * 获取指定类型的对象
     * @param request http请求
     * @param cla  类型
     * @return  bean对象
     */
    public static <T> T getBean(HttpServletRequest request, Class<T> cla) {
        return getWebApplicationContext(request).getBean(cla);
    }
}

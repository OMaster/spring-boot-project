package cn.orangebright.spring.util;

import java.util.Locale;

import cn.orangebright.spring.service.ApplicationContextService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 获取i18n资源文件。
 * 默认情况：读取的是 resource 目录下的 messages.properties文件。可以通过spring.messages.basename属性修改目录和文件名称：
 * <ol>
 *     <li>修改文件名称，使其读取resource目录下的login.properties文件：login</li>
 *     <li>修改文件目录，使其读取resource目录下ms目录中的messages.properties文件：ms/messages</li>
 *     <li>修改文件目录和文件名称，使其读取resource目录下ms目录中的login.properties文件：ms/login</li>
 * </ol>
 * filename/messages
 *
 * @author orangebright
 * @date 2023/8/15
 */
public class MessageUtil {

    public static final Locale LOCALE = LocaleContextHolder.getLocale();

    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息代码
     * @param args 消息参数
     * @return 代码对应的本地环境语言的消息
     */
    /**
     *
     * @param code
     * @param args
     * @return
     */
    public static String message(String code, Object... args) {
        MessageSource messageSource = ApplicationContextService.getBean(MessageSource.class);
        return messageSource.getMessage(code, args, LOCALE);
    }
}

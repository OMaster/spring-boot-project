package cn.orangebright.common.request.repeat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import cn.orangebright.common.request.RequestUtil;

/**
 * 可重复读的请求
 *
 * @author snowwalker
 * @date 2023/10/11
 */
public class RepeatableHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private final byte[] body;

    public RepeatableHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
//        body = RequestUtil.getRequestBody(request);
        body = RequestUtil.getStringRequestBody(request).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new RepeatableReadInputStream(body);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream(), StandardCharsets.UTF_8));
    }

}

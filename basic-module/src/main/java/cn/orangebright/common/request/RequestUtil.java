package cn.orangebright.common.request;

import java.io.BufferedReader;
import java.io.IOException;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.request.repeat.RepeatableHttpServletRequestWrapper;
import cn.orangebright.common.util.JsonUtil;
import cn.orangebright.common.util.RegionUtil;
import eu.bitwalker.useragentutils.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 请求工具类
 * @author orangebright
 * @date 2022/7/14
 */
public class RequestUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(RequestUtil.class);

    /**
     * 获取当前请求中的参数字符串
     * @param request
     * @return
     */
    public static String getRequestParam(HttpServletRequest request) {
        if (request instanceof RepeatableHttpServletRequestWrapper) {
            return getStringRequestBody(request);
        } else {
            return JsonUtil.objectToString(request.getParameterMap());
        }
    }

    /**
     * 按字节读取请求体，会包含\r\n
     * @param request
     * @return
     */
    public static byte[] getRequestBody(HttpServletRequest request) {
        try (ServletInputStream inputStream = request.getInputStream()) {
            return FileCopyUtils.copyToByteArray(inputStream);
        } catch (IOException e) {
            LOGGER.error("获取请求[{}]的请求体参数失败", request.getRequestURI(), e);
        }
        return new byte[0];
    }

    /**
     * 按行读取请求体，不包含\r\n
     * @param request
     * @return
     */
    public static String getStringRequestBody(HttpServletRequest request) {
        StringBuilder data = new StringBuilder();
        try (BufferedReader reader = request.getReader()) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                data.append(line);
            }
        } catch (IOException e) {
            LOGGER.error("获取请求[{}]的请求体参数失败", request.getRequestURI(), e);
        }
        return data.toString();
    }

    /**
     * 获取操作系统名称
     * @return
     */
    public static String getOs() {
        return getUserAgent().getOperatingSystem().getName();
    }

    /**
     * 获取浏览器内核名称
     * @return
     */
    public static String getBrowser() {
        return getUserAgent().getBrowser().getName();
    }

    public static UserAgent getUserAgent() {
        return getUserAgent(RequestUtil.getRequest());
    }

    public static UserAgent getUserAgent(HttpServletRequest request) {
        String userAgent = request == null ? null : request.getHeader("User-Agent");
        return UserAgent.parseUserAgentString(userAgent);
    }

    /**
     * 获取request
     * @return request
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = getRequestAttributes();
        return requestAttributes == null ? null : requestAttributes.getRequest();
    }

    /**
     * 获取response
     * @return response
     */
    public static HttpServletResponse getResponse() {
        ServletRequestAttributes requestAttributes = getRequestAttributes();
        return requestAttributes == null ? null : requestAttributes.getResponse();
    }

    public static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) requestAttributes;
    }

    public static String getAddress() {
        return RegionUtil.getRegion(getRemoteIp());
    }

    /**
     * 获取请求ip
     * @return ip
     */
    public static String getRemoteIp() {
        return getRemoteIp(getRequest());
    }

    /**
     * 根据request获取ip
     * @param request http请求
     * @return  ip地址
     */
    public static String getRemoteIp(HttpServletRequest request) {
        if (request == null) {
            return "";
        }
        String ip = request.getHeader("x-forward-for");
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_Client_IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip != null && ip.contains(",")) {
            ip = ip.substring(0, ip.indexOf(","));
        }
        return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
    }
}

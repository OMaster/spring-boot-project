package cn.orangebright.common.request.repeat;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.util.ContentCachingRequestWrapper;

/**
 * 构建可重复读请求体的请求（只对 <strong>application/json</strong> 请求生效）
 * <p>{@link ContentCachingRequestWrapper}：需要第一次通过{@link ContentCachingRequestWrapper#getInputStream()}方法将数据读到缓存中，
 * 之后再读取时通过{@link ContentCachingRequestWrapper#getContentAsByteArray()}从缓存中读取数据（不适用于ruoyi中的情形）
 *
 * @author snowwalker
 * @date 2023/10/11
 */
public class RepeatableFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest httpRequest
                && StringUtils.startsWithIgnoreCase(request.getContentType(), MediaType.APPLICATION_JSON_VALUE)) {
            request = new RepeatableHttpServletRequestWrapper(httpRequest);
        }
        chain.doFilter(request, response);
    }
}

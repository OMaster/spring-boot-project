package cn.orangebright.common.request.repeat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;

import org.springframework.util.Assert;

/**
 * 可重复读的InputStream
 *
 * @author snowwalker
 * @date 2023/10/11
 */
public class RepeatableReadInputStream extends ServletInputStream {

    private final int length;
    private final ByteArrayInputStream bais;

    public RepeatableReadInputStream(byte[] data) {
        Assert.notNull(data, "byte array must not be null");
        length = data.length;
        bais = new ByteArrayInputStream(data);
    }

    @Override
    public int read() throws IOException {
        return bais.read();
    }

    @Override
    public int available() throws IOException {
        return length;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setReadListener(ReadListener listener) {

    }

}

package cn.orangebright.common.threadpool;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 线程池工具
 *
 * @author orangebright
 * @date 2023/8/15
 */
public abstract class ThreadPoolUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadPoolUtil.class);

    /**
     * 等待任务执行时间：60s
     */
    public static final int awaitTerminationSeconds = 60;

    /**
     * 记录线程池执行异常
     * @param r 任务
     * @param t 异常
     */
    public static void recordException(Runnable r, Throwable t) {
        if (r instanceof FutureTask<?> ft) {
            try {
                ft.get();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                t = e.getCause();
            } catch (Exception e) {
                LOGGER.error("异步任务异常：", e);
            }
        }
        if (t != null) {
            LOGGER.error("异步任务异常：", t);
        }
    }

    /**
     * 停止线程池：
     * <ol>
     *     <li>先使用shutdown, 停止接收新任务并尝试完成队列中的所有任务.</li>
     *     <li>如果超时, 则调用shutdownNow, 取消在workQueue中Pending的任务,并中断所有阻塞函数.</li>
     *     <li>如果仍然超時，則強制退出. </li>
     *     <li>另对在shutdown时线程本身被调用中断做了处理.</li>
     * </ol>
     *
     * @param executor
     */
    public static void shutdown(ExecutorService executor) {
        // 为null，或已经关闭，直接返回
        if (executor == null || executor.isShutdown()) {
            return;
        }
        // 1、调用 shutdown()，线程池进入 SHUTDOWN 状态，处理队列中的所有任务
        executor.shutdown();
        try {
            // 60s内队列中的任务没有处理完，则调用 shutdownNow()
            if (!executor.awaitTermination(awaitTerminationSeconds, TimeUnit.SECONDS)) {
                // 调用 shutdownNow()，线程池进入 STOP 状态，中断正在处理的任务
                shutdownNow(executor);
                // 60s内线程池中还有线程，记录日志，调用 terminated()，强制退出，线程池进入 TERMINATED 状态
                if (!executor.awaitTermination(awaitTerminationSeconds, TimeUnit.SECONDS)) {
                    LOGGER.warn("线程池任务未正常执行结束");
                }
            }
        } catch (InterruptedException e) {
            // 第一个if抛异常，则不会调用 shutdownNow()，因此，这里需要重写调用
            shutdownNow(executor);
            // 中断当前线程
            Thread.currentThread().interrupt();
        }
        LOGGER.warn("线程池任务正常执行结束");
    }

    /**
     * 调用线程池 shutdownNow() 方法，进入 STOP 状态，并取消队列中等待的任务
     */
    private static void shutdownNow(ExecutorService executor) {
        for (Runnable runnable : executor.shutdownNow()) {
            cancelRemainingTask(runnable);
        }
    }

    /**
     * 取消任务
     * @param task  任务
     */
    private static void cancelRemainingTask(Runnable task) {
        if (task instanceof Future) {
            ((Future<?>) task).cancel(true);
        }
    }

}

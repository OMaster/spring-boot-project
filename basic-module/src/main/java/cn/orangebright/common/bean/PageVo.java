package cn.orangebright.common.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 分页vo，分页查询的vo父类
 * <li>{@link JsonIgnore}：序列化和反序列化时都忽略指定字段</li>
 * <li>{@link JsonProperty}：指定序列化和反序列化方式。{@link JsonProperty#access()}方法属性值如下：</li>
 * <ol>
 *     <li>{@link JsonProperty.Access#READ_ONLY}：只读，也就是说仅仅为反序列化（字符串->对象）时指定字段生效</li>
 *     <li>{@link JsonProperty.Access#WRITE_ONLY}：只写，也就是说仅仅为序列化（对象->字符串）时指定字段生效</li>
 *     <li>{@link JsonProperty.Access#AUTO}、{@link JsonProperty.Access#READ_WRITE}：序列化和反序列化都生效</li>
 * </ol>
 * 两个注解同时使用时，{@link JsonIgnore}的优先级高，及均忽略指定字段
 * @author snowwalker
 * @date 2023/10/18
 */
@Data
public class PageVo {

    /**
     * 主键（可能为整数型的自增主键，或UUID）
     * <p>区别：
     * <li>如果是单机环境，对性能有较高要求，且不需要考虑分布式扩展或数据迁移等问题，自增主键是一个更好的选择。</li>
     * <li>对于分布式系统、需要跨节点生成唯一ID或者未来可能需要拆分数据库的情况，使用UUID作为主键更能保证全局唯一性，但要考虑到存储和性能上的牺牲。</li>
     */
    private Object id;

    /**
     * 当前页
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long pageIndex = 1;

    /**
     * 分页大小（序列化时忽略该字段。仅反序列化时有效）
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long pageSize = 10;

}

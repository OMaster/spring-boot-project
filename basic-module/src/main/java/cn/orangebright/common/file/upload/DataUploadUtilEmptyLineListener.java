package cn.orangebright.common.file.upload;

import java.util.List;
import java.util.function.Consumer;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.enums.RowTypeEnum;

/**
 * 数据上传监听器，遇到空行时停止上传
 *
 * @author snowwalker
 * @date 2023/9/14
 */
public class DataUploadUtilEmptyLineListener<T> extends DataUploadListener<T> {

    public DataUploadUtilEmptyLineListener(Consumer<List<T>> consumer) {
        super(consumer);
    }

    public DataUploadUtilEmptyLineListener(int cacheDataSize, Consumer<List<T>> consumer) {
        super(cacheDataSize, consumer);
    }

    /**
     * 必须设置为不忽略空行，遇到空行时easyexcel正常处理。这样我们才能通过{@link #hasNext(AnalysisContext)}方法来终止上传
     *
     * @param data
     * @param context
     * @see com.alibaba.excel.read.processor.DefaultAnalysisEventProcessor#endRow(AnalysisContext)
     * @see com.alibaba.excel.read.processor.DefaultAnalysisEventProcessor#dealData(AnalysisContext)
     */
    @Override
    public void invoke(T data, AnalysisContext context) {
        // 不忽略空行
        context.readWorkbookHolder().setIgnoreEmptyRow(false);
        // 是空行，解析完成，hasNext返回false，会抛出异常，导致不会自动执行doAfterAllAnalysed()，故需要在这里手动执行
        if (isEmptyRow(context)) {
            doAfterAllAnalysed(context);
            return;
        }
        super.invoke(data, context);
    }

    @Override
    public boolean hasNext(AnalysisContext context) {
        // 空行，不继续往下读数据
        if (isEmptyRow(context)) {
            return false;
        }
        return super.hasNext(context);
    }

    private boolean isEmptyRow(AnalysisContext context) {
        return RowTypeEnum.EMPTY.equals(context.readRowHolder().getRowType());
    }
}

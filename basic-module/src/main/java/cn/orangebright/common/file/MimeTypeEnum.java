package cn.orangebright.common.file;

import java.util.HashMap;
import java.util.Map;

import cn.orangebright.common.exception.ServiceException;
import org.springframework.util.StringUtils;

/**
 * 多媒体类型枚举
 *
 * @author orangebright
 * @date 2021/12/16
 */
public enum MimeTypeEnum {

    /**
     * word文档：doc文件、docx文件
     */
    DOC(".doc", "application/msword"),
    DOCX(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),

    /**
     * 演示文档：ppt文件、pptx文件
     */
    PPT(".ppt", "application/vnd.ms-powerpoint"),
    PPTX(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),

    /**
     * excel文档：xls文件、xlsx文件
     */
    XLS(".xls", "application/vnd.ms-excel"),
    XLSX(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),

    /**
     * pdf文档：pdf文件
     */
    PDF(".pdf", "application/pdf"),

    /**
     * 图片文件：gif、png、jpg、jpeg
     */
    GIF(".gif", "image/gif"),
    PNG(".png", "image/png"),
    JPG(".jpg", "image/jpeg"),
    JPEG(".jpeg", "image/jpeg"),
    ICO(".ico", "image/vnd.microsoft.icon"),

    /**
     * 视频文件：mp4文件、avi文件
     */
    MP4(".mp4", "video/mp4"),
    AVI(".avi", "video/x-msvideo"),

    /**
     * 压缩文件：zip文件、rar文件、7z文件
     */
    ZIP(".zip", "application/zip"),
    RAR(".rar", "application/vnd.rar"),
    SEVEN_Z(".7z", "application/x-7z-compressed"),

    /**
     * 文本类型：txt文件、html文件
     */
    TXT(".txt", "text/plain"),
    HTML(".html", "text/html"),

    /**
     * json数据类型
     */
    JSON(".json", "application/json")
    ;


    private final String extension;

    private final String mimeType;

    private static final Map<String, MimeTypeEnum> MIME_TYPE_MAP = new HashMap<>();

    static {
        for (MimeTypeEnum mimeTypeEnum : MimeTypeEnum.values()) {
            MIME_TYPE_MAP.put(mimeTypeEnum.extension, mimeTypeEnum);
        }
    }

    MimeTypeEnum(String extension, String mimeType) {
        this.extension = extension;
        this.mimeType = mimeType;
    }

    /**
     * 通过文件名获取对应的MIME类型
     * @param fileName 文件名
     * @return MIME类型
     */
    public static String getMimeTypeByFilename(String fileName) {
        return getMimeType(fileName.substring(fileName.lastIndexOf('.')));
    }

    /**
     * 通过Map集合初始化数据，提高查找效率，从而避免每次都调用 values()
     * @param extension 扩展名
     * @return 对应的MIME类型
     */
    public static String getMimeType(String extension) {
        MimeTypeEnum mimeTypeEnum = getMimeTypeEnum(extension);
        if (mimeTypeEnum == null) {
            throw new ServiceException("无对应文件的MIME类型");
        }
        return mimeTypeEnum.mimeType;
    }

    public static MimeTypeEnum getMimeTypeEnum(String extension) {
        return MIME_TYPE_MAP.get(extension.toLowerCase());
    }

    /**
     * 通过Map集合初始化数据，提高查找效率，从而避免每次都调用 values()
     * @param fileName 文件名
     * @return
     */
    @Deprecated
    public static String getMimeType2(String fileName) {
        for (MimeTypeEnum mimeTypeEnum : MimeTypeEnum.values()) {
            if (StringUtils.endsWithIgnoreCase(fileName, mimeTypeEnum.extension)) {
                return mimeTypeEnum.mimeType;
            }
        }
        throw new ServiceException("无对应文件的MIME类型");
    }

    public static boolean isExcel(String fileName) {
        return StringUtils.endsWithIgnoreCase(fileName, MimeTypeEnum.XLS.extension)
            || StringUtils.endsWithIgnoreCase(fileName, MimeTypeEnum.XLSX.extension);
    }

    public String getExtension() {
        return extension;
    }

    public String getMimeType() {
        return mimeType;
    }
}

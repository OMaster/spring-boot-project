package cn.orangebright.common.file;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 压缩包工具
 * @author orangebright
 * @date 2023/5/12
 */
public class ZipUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZipUtil.class);

    /**
     * 导出压缩文件
     * @param fileName  文件名
     * @param fileList  文件条目
     * @param response
     */
    public static void export(String fileName, List<FileEntry> fileList, HttpServletResponse response) {
        FileUtil.responseConfig(fileName, response);
        try (ServletOutputStream sos = response.getOutputStream()) {
            compress(sos, fileList);
        } catch (IOException e) {
            LOGGER.error("导出压缩文件{}失败", fileName, e);
        }
    }


    /**
     * 压缩文件条目到指定流中
     * @param outputStream  输出流
     * @param fileList  文件条目
     */
    public static void compress(OutputStream outputStream, List<FileEntry> fileList) {
        // close()方法中隐式调用finish()方法，完成数据写入。
        // 如果需要在close()方法前获取所有数据，则需在获取前显式调用finish()方法，这样才能保证数据完整性
        try (ZipOutputStream zos = new ZipOutputStream(outputStream)) {
            zos.setMethod(ZipOutputStream.DEFLATED);
            // 压缩级别值为0-9共10个级别(值越大，表示压缩越利害)
            zos.setLevel(Deflater.BEST_COMPRESSION);
            for (FileEntry fileEntry : fileList) {
                if (fileEntry.getFileByte() != null) {
                    // 设置ZipEntry对象，并对需要压缩的文件命名
                    zos.putNextEntry(new ZipEntry(fileEntry.getFileName()));
                    zos.write(fileEntry.getFileByte());
                }
                // 创建空目录条目
                else {
                    // 文件名称必须以文件分隔符结尾，才会将其解析为目录
                    String fileName = fileEntry.getFileName();
                    if (!fileName.endsWith(File.separator)) {
                        fileName += File.separator;
                    }
                    // 空目录，大小为0，没必要压缩
                    ZipEntry emptyEntry = new ZipEntry(fileName);
                    emptyEntry.setSize(0);
                    emptyEntry.setCrc(0);
                    emptyEntry.setMethod(ZipEntry.STORED);
                    zos.putNextEntry(emptyEntry);
                }
                // 关闭当前条目（建议显式调用，保证流程可见性）
                // 可选，会隐式调用：putNextEntry()中会隐式调用关闭上一个Entry，最后一个Entry会在finish()方法中隐式调用
                zos.closeEntry();
            }
        } catch (IOException e) {
            LOGGER.error("文件压缩失败");
        }
    }


    /**
     * 文件对象
     * @author orangebright
     * @date 2023/5/12
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FileEntry {

        /**
         * 文件名称
         */
        private String fileName;

        /**
         * 文件对应的字节数组
         */
        private byte[] fileByte;

    }
}

package cn.orangebright.common.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

/**
 * 资源目录下的文件导出工具类
 * @author snowwalker
 * @date 2023/6/27
 */
public class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    private static final Pattern PATTERN = Pattern.compile("\\+");

    /**
     * 导出指定路径下的文件
     * @param filePath 文件路径
     * @param response 响应对象
     */
    public static void exportFile(String filePath, HttpServletResponse response) {
        responseConfig(filePath, response);
        try (ServletOutputStream sos = response.getOutputStream()) {
            Path path = new File(filePath).toPath();
            FileCopyUtils.copy(Files.newInputStream(path), sos);
        } catch (IOException e) {
            LOGGER.error("{}文件导出失败：", filePath, e);
        }
    }

    /**
     * 导出resources下的文件
     * @param filename 文件路径，若文件位于file目录下，则filename的值为 "file/a.xlsx"
     * @param response response对象
     */
    public static void exportResourceFile(String filename, HttpServletResponse response) {
        responseConfig(filename, response);
        ClassPathResource classPathResource = new ClassPathResource(filename);
        try (ServletOutputStream sos = response.getOutputStream();
             InputStream inputStream = classPathResource.getInputStream()) {
            FileCopyUtils.copy(inputStream, sos);
        } catch (IOException e) {
            LOGGER.error("{}文件导出失败：", filename, e);
        }
    }

    /**
     * 导出resources下的文件（jar包启动的项目不能正常导出）
     * @param filename 文件路径
     * @param response response对象
     */
    @Deprecated
    public static void exportTemplate(String filename, HttpServletResponse response) {
        responseConfig(filename, response);
        // jar包中的资源不在文件系统中，故获取File对象时为null，不能正常导出
        ClassPathResource classPathResource = new ClassPathResource(filename);
        try (ServletOutputStream sos = response.getOutputStream()) {
            File file = classPathResource.getFile();
            sos.write(FileUtils.readFileToByteArray(file));
            sos.flush();
        } catch (IOException e) {
            LOGGER.error("{}文件导出失败：", filename, e);
        }
    }

    /**
     * 通过workbook对象导出文件（针对Excel文件有效）
     * @param filename 文件路径
     * @param response response对象
     * @deprecated 直接使用 {@link #exportResourceFile(String, HttpServletResponse)} 方法导出资源目录文件
     */
    @Deprecated
    public static void exportExcel(String filename, HttpServletResponse response) {
        if (!MimeTypeEnum.isExcel(filename)) {
            LOGGER.warn("ExportUtil.exportByPoi()方法只能接受excel文件");
            return;
        }
        responseConfig(filename, response);
        ClassPathResource classPathResource = new ClassPathResource(filename);
        try (ServletOutputStream sos = response.getOutputStream();
             InputStream inputStream = classPathResource.getInputStream();
             Workbook workbook = WorkbookFactory.create(inputStream)) {
            workbook.write(sos);
            sos.flush();
        } catch (IOException e) {
            LOGGER.error("{}文件导出失败：", filename, e);
        }
    }

    /**
     * 设置响应头
     * @param filename 文件目录名称
     * @param response response对象
     */
    public static void responseConfig(String filename, HttpServletResponse response) {
        response.setContentType(MimeTypeEnum.getMimeTypeByFilename(filename));
        if (filename.contains(File.separator)) {
            filename = filename.substring(filename.lastIndexOf(File.separatorChar) + 1);
        }
        filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        filename = PATTERN.matcher(filename).replaceAll("%20");
        response.setHeader("Content-Disposition", "attachment;filename*=utf-8''" + filename);
    }

    /**
     * 测试发现火狐浏览器也可以解析采用URLEncoder编码的文件名，故无需判断请求头的user-agent属性
     * @param filename 文件目录名称
     * @param request  request对象
     * @param response response对象
     */
    @Deprecated
    public static void firefoxHeader(String filename, HttpServletRequest request, HttpServletResponse response) {
        String agent = request.getHeader("USER-AGENT");
        if (agent != null && agent.contains("Firefox")) {
            filename = new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
        }
        else {
            filename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
        }
        response.setHeader("Content-Disposition", "attachment;filename*=utf-8''" + filename);
    }
}

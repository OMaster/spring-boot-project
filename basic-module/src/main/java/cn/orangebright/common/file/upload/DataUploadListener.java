package cn.orangebright.common.file.upload;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import org.springframework.util.Assert;

/**
 * 数据上传监听器
 * <p>{@link com.alibaba.excel.annotation.ExcelProperty}用法：导入数据，用名称（value属性）匹配；导出数据，用order指定顺序</p>
 *
 * @author snowwalker
 * @date 2023/9/6
 */
public class DataUploadListener<T> extends AnalysisEventListener<T> {

    public static final int DEFAULT_CACHE_DATA_SIZE = 900;

    /**
     * 单次读取的数据大小
     */
    private int cacheDataSize = DEFAULT_CACHE_DATA_SIZE;

    /**
     * 单次读取的数据集
     */
    private List<T> cacheDataList;

    /**
     * 数据批量入库方法
     */
    private Consumer<List<T>> consumer;

    public DataUploadListener(Consumer<List<T>> consumer) {
        this(DEFAULT_CACHE_DATA_SIZE, consumer);
    }

    public DataUploadListener(int cacheDataSize, Consumer<List<T>> consumer) {
        Assert.notNull(consumer, "数据入库方法不能为空");
        this.cacheDataSize = cacheDataSize;
        cacheDataList = new ArrayList<>(cacheDataSize);
        this.consumer = consumer;
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        // TODO: 2023/9/6 获取表格头：增加校验等操作
        super.invokeHeadMap(headMap, context);
    }

    @Override
    public void invoke(T data, AnalysisContext context) {
        cacheDataList.add(data);
        if (cacheDataList.size() >= cacheDataSize) {
            // 入库
            dataHandler();
            // 清空集合。对于大量数据，clear()方法可能很慢
            cacheDataList = new ArrayList<>(cacheDataSize);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 将最后一批数据入库
        dataHandler();
    }

    private void dataHandler() {
        consumer.accept(cacheDataList);
    }
}

package cn.orangebright.common.file;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 导出Excel文件
 *
 * @author snowwalker
 * @date 2023/6/27
 */
public class ExcelUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * 分页导出大小：1w
     */
    public static final int PAGE_SIZE = 10000;

    /**
     * 将excel文件写入输出流
     *
     * @param fileName 文件名
     * @param workbook excel对象
     * @param response
     */
    public static void export(String fileName, Workbook workbook, HttpServletResponse response) {
        FileUtil.responseConfig(fileName, response);
        try (ServletOutputStream sos = response.getOutputStream(); workbook) {
            workbook.write(sos);
            sos.flush();
            // 删除磁盘备份文件
            if (workbook instanceof SXSSFWorkbook sxssfWorkbook) {
                sxssfWorkbook.dispose();
            }
        } catch (IOException e) {
            LOGGER.error("导出{}信息异常：", fileName, e);
        }
    }

    /**
     * 处理分页数据
     * @param headers   excel表格头
     * @param dataQuery     数据查询接口
     * @param dataAppend    数据拼接接口
     * @return  Workbook
     * @param <T>   数据类型
     */
    public static <T> Workbook handlePageData(String[] headers, Function<Integer, List<T>> dataQuery, BiConsumer<Row, T> dataAppend) {
        Workbook wb = new SXSSFWorkbook();
        Sheet sh = wb.createSheet();
        int rows = 0;
        Row row = sh.createRow(rows++);
        ExcelUtil.initTableHeader(row, setCellStyle(wb, sh), headers);
        int pageIndex = 1;
        while (true) {
            List<T> dataList = dataQuery.apply(pageIndex++);
            if (dataList.isEmpty()) {
                break;
            }
            for (T data : dataList) {
                row = sh.createRow(rows++);
                dataAppend.accept(row, data);
            }
        }
        return wb;
    }

    /**
     * 设置表格头
     *
     * @param row     行对象
     * @param headers 表格头数组
     */
    public static void initTableHeader(Row row, CellStyle cellStyle, String[] headers) {
        for (int i = 0; i < headers.length; i++) {
            row.createCell(i, CellType.STRING).setCellStyle(cellStyle);
            row.getCell(i).setCellValue(headers[i]);
        }
    }

    /**
     * 设置表格数据，取值范围：[0, end]
     *
     * @param row 行对象
     * @param vo  数据对象
     * @param end 结束数据索引
     * @return 列索引
     */
    public static int appendData(Row row, Object[] vo, int end) {
        for (int i = 0; i <= end; i++) {
            row.createCell(i, CellType.STRING).setCellValue(Objects.toString(vo[i], null));
        }
        return end;
    }

    /**
     * 设置表格数据，取值范围：[begin, end]
     *
     * @param row   行对象
     * @param vo    数据对象
     * @param index 列索引
     * @param begin 起始数据索引
     * @param end   结束数据索引
     * @return 列索引
     */
    public static int appendData(Row row, Object[] vo, int index, int begin, int end) {
        for (int i = begin; i <= end; i++) {
            row.createCell(index++, CellType.STRING).setCellValue(Objects.toString(vo[i], null));
        }
        return index;
    }

    /**
     * Excel表格头样式
     * @param wb    excel对象
     * @param sh    sheet页对象
     * @return  表格样式
     */
    public static CellStyle setCellStyle(Workbook wb, Sheet sh) {
        // 设置行高:256 * 1.5、列宽
        sh.setDefaultRowHeight((short) 384);
        sh.setDefaultColumnWidth(15);
        // 设置表头样式
        CellStyle cs = wb.createCellStyle();
        Font font = wb.createFont();
        font.setBold(true);
        font.setFontName("宋体");
        cs.setFont(font);
        return cs;
    }
}

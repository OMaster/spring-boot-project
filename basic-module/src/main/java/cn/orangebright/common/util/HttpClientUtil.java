package cn.orangebright.common.util;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactoryBuilder;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.apache.hc.core5.util.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Apache HttpClient工具类
 * @author snowwalker
 * @date 2022/9/8
 */
public class HttpClientUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpClientUtil.class);

    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    //---------------------------------------------------------------------------get request

    /**
     * get请求
     * <p>
     * 如果请求参数包含中文，则必须<strong>先对中文参数进行转码，然后再拼接到url中</strong>，
     * 详见{@linkplain java.net.URLEncoder#encode(String, String) URLEncoder.encode}
     * </p>
     * @param url 请求路径
     * @return 请求结果
     */
    public static String get(String url) {
        return get(url, null);
    }

    /**
     * get请求
     * <p>
     * 如果请求参数包含中文，则必须<strong>先对中文参数进行转码，然后再拼接到url中</strong>，
     * 详见{@linkplain java.net.URLEncoder#encode(String, String) URLEncoder.encode}
     * </p>
     * @param url     请求路径
     * @param headers 请求头
     * @return 请求结果
     */
    public static String get(String url, Map<String, String> headers) {
        if (!StringUtils.hasText(url)) {
            return "";
        }
        HttpGet httpGet = new HttpGet(url);
        return request(httpGet, headers);
    }

    //---------------------------------------------------------------------------post request

    /**
     * post请求：设置json参数
     * @param url 请求路径
     * @param obj 参数对象
     * @return 请求结果
     */
    public static String postJson(String url, Object obj) {
        return postJson(url, JsonUtil.objectToString(obj));
    }

    /**
     * post请求：设置json参数
     * @param url       请求路径
     * @param jsonParam json串
     * @return 请求结果
     */
    public static String postJson(String url, String jsonParam) {
        HttpPost httpPost = new HttpPost(url);
        // 设置参数并指定编码
        if (jsonParam != null) {
            httpPost.setEntity(new StringEntity(jsonParam, ContentType.APPLICATION_JSON));
        }
        return request(httpPost);
    }

    /**
     * 代理请求
     * @param url       请求路径
     * @param headers   请求头
     * @param proxyHost 代理主机
     * @param proxyPort 代理主机接口
     * @return 请求结果
     */
    public static String postJsonProxy(String url, Map<String, String> headers, String jsonParam, String proxyHost, int proxyPort) {
        HttpPost httpPost = new HttpPost(url);
        // 设置参数并指定编码
        if (jsonParam != null) {
            httpPost.setEntity(new StringEntity(jsonParam, ContentType.APPLICATION_JSON));
        }
        httpPost.setConfig(requestConfig(new HttpHost(proxyHost, proxyPort)));
        return request(httpPost, headers);
    }

    public static String post(String url) {
        return request(new HttpPost(url));
    }

    public static String post(String url, Map<String, String> params) {
        return post(url, null, params);
    }

    /**
     * post请求：通过ClassicRequestBuilder创建ClassicHttpRequest对象，并设置参数（将参数放到请求体中）
     * @param url     请求路径
     * @param headers 请求头
     * @param params  请求参数
     * @return 请求结果
     */
    public static String post(String url, Map<String, String> headers, Map<String, String> params) {
        if (!StringUtils.hasText(url)) {
            return "";
        }
        ClassicRequestBuilder postBuilder = ClassicRequestBuilder.post(url);
        // 设置参数并指定编码
        if (params != null) {
            List<NameValuePair> nvps = new ArrayList<>();
            params.forEach((key, value) -> nvps.add(new BasicNameValuePair(key, value)));
            postBuilder.setEntity(new UrlEncodedFormEntity(nvps, DEFAULT_CHARSET));
        }
        return request(postBuilder.build(), headers);
    }

    /**
     * post请求：通过ClassicRequestBuilder创建ClassicHttpRequest对象，并设置参数（将参数追加到url后面）
     * @param url     请求路径
     * @param headers 请求头
     * @param params  请求参数
     * @return 请求结果
     */
    public static String post2(String url, Map<String, String> headers, Map<String, String> params) {
        if (!StringUtils.hasText(url)) {
            return "";
        }
        ClassicRequestBuilder postBuilder = ClassicRequestBuilder.post(url);
        // 设置参数并指定编码
        if (params != null) {
            params.forEach(postBuilder::addParameter);
            postBuilder.setCharset(DEFAULT_CHARSET);
        }
        return request(postBuilder.build(), headers);
    }

    /**
     * post代理请求，
     * 设置代理信息时不能使用{@link ClassicRequestBuilder}构造请求，
     * 必须使用{@linkplain  HttpUriRequestBase HttpUriRequestBase}类型构造请求
     * @param url       请求路径
     * @param headers   请求头
     * @param proxyHost 代理主机
     * @param proxyPort 代理主机接口
     * @return 请求结果
     */
    public static String postProxy(String url, Map<String, String> headers, Map<String, String> params, String proxyHost, int proxyPort) {
        HttpPost httpPost = new HttpPost(url);
        // 设置参数并指定编码
        if (params != null) {
            List<NameValuePair> nvps = new ArrayList<>();
            params.forEach((key, value) -> nvps.add(new BasicNameValuePair(key, value)));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, DEFAULT_CHARSET));
        }
        httpPost.setConfig(requestConfig(new HttpHost(proxyHost, proxyPort)));
        return request(httpPost, headers);
    }

    /**
     * 文件上传的post请求
     * @param url         请求路径
     * @param stringParam 字符串参数
     * @param fileParam   文件参数
     * @return 请求结果
     */
    public static String postFile(String url, Map<String, String> stringParam, Map<String, File> fileParam) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        if (stringParam != null) {
            stringParam.forEach(builder::addTextBody);
        }
        if (fileParam != null) {
            fileParam.forEach(builder::addBinaryBody);
        }
        return postEntity(url, builder.build());
    }

    /**
     * 通用post请求
     * @param url    请求路径
     * @param entity 请求体
     * @return 请求结果
     */
    public static String postEntity(String url, HttpEntity entity) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(entity);
        return request(httpPost);
    }

    /**
     * 通用post代理请求
     * @param url       请求路径
     * @param entity    请求体
     * @param proxyHost 代理主机
     * @return 请求结果
     */
    public static String postEntityProxy(String url, HttpEntity entity, HttpHost proxyHost) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(entity);
        return proxyRequest(httpPost, proxyHost);
    }

    //---------------------------------------------------------------------------proxy request

    /**
     * 通用代理请求方法
     * @param httpRequest 请求对象
     * @param proxyHost   请求代理主机
     * @return
     */
    public static String proxyRequest(HttpUriRequestBase httpRequest, HttpHost proxyHost) {
        return proxyRequest(httpRequest, requestConfig(proxyHost));
    }

    /**
     * 通用代理请求方法
     * @param httpRequest 请求对象
     * @param config      请求配置
     * @return 请求结果
     */
    public static String proxyRequest(HttpUriRequestBase httpRequest, RequestConfig config) {
        httpRequest.setConfig(config);
        return request(httpRequest);
    }


    /**
     * 默认的请求配置
     * @param proxyHost 代理主机
     * @return 请求配置
     */
    private static RequestConfig requestConfig(HttpHost proxyHost) {
        return RequestConfig.custom()
                .setProxy(proxyHost)
                .setConnectTimeout(5, TimeUnit.SECONDS)
                .setResponseTimeout(Timeout.ofSeconds(3))
                .build();
    }

    //---------------------------------------------------------------------------basic request

    /**
     * 通用请求方法：先设置请求头然后调用接口
     * @param httpRequest 请求对象
     * @param headers     请求头信息
     * @return 请求结果
     */
    public static String request(ClassicHttpRequest httpRequest, Map<String, String> headers) {
        if (headers != null) {
            headers.forEach(httpRequest::setHeader);
        }
        return request(httpRequest);
    }

    /**
     * 通用请求方法
     * @param httpRequest 请求对象
     * @return 请求结果
     */
    public static String request(ClassicHttpRequest httpRequest) {
        return request(httpRequest, false);
    }

    /**
     * 通用请求方法
     * @param httpRequest 请求对象
     * @param isHttps     是否为https请求
     * @return 请求结果
     */
    public static String request(ClassicHttpRequest httpRequest, boolean isHttps) {
        try (CloseableHttpClient httpClient = isHttps ? getHttpsClient() : getHttpClient()) {
            try (CloseableHttpResponse response = httpClient.execute(httpRequest)) {
                if (response.getCode() == HttpStatus.SC_OK) {
                    HttpEntity entity = response.getEntity();
                    String body = EntityUtils.toString(entity, DEFAULT_CHARSET);
                    EntityUtils.consume(entity);
                    return body;
                }
                LOGGER.warn("response code: " + response.getCode() + ";response body: " + response.getEntity());
            }
        } catch (Exception e) {
            LOGGER.error("请求" + httpRequest.getPath() + "}错误", e);
        }
        return "";
    }


    /**
     * 获取忽略SSL证书的httpsClient对象，如果获取失败，则返回默认的HttpClient
     * @return
     */
    public static CloseableHttpClient getHttpsClient() {
        try {
            SSLContext sslContext = SSLContextBuilder.create().loadTrustMaterial((chain, authType) -> true).build();
            SSLConnectionSocketFactory socketFactory = SSLConnectionSocketFactoryBuilder.create()
                    .setSslContext(sslContext)
                    .setHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();
            PoolingHttpClientConnectionManager connectionManager = PoolingHttpClientConnectionManagerBuilder.create()
                    .setSSLSocketFactory(socketFactory).build();
            return HttpClients.custom().setConnectionManager(connectionManager).build();
        } catch (Exception e) {
            LOGGER.error("获取httpsClient实例失败", e);
            return getHttpClient();
        }
    }

    /**
     * 获取httpClient对象
     * @return
     */
    public static CloseableHttpClient getHttpClient() {
        return HttpClients.createDefault();
    }

}

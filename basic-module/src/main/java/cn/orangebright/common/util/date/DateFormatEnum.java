package cn.orangebright.common.util.date;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日期格式化枚举
 * @author snowwalker
 * @date 2024/2/22
 */
public enum DateFormatEnum {

    DATE_PATTERN("yyyy-MM-dd") {
        @Override
        public ThreadLocal<SimpleDateFormat> threadLocalObject() {
            return ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_PATTERN.pattern));
        }
    },

    DATE_TIME_PATTERN("yyyy-MM-dd HH:mm:ss") {
        @Override
        public ThreadLocal<SimpleDateFormat> threadLocalObject() {
            return ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_TIME_PATTERN.pattern));
        }
    },
    ;

    private static final Logger LOGGER = LoggerFactory.getLogger(DateFormatEnum.class);

    private final String pattern;

    public abstract ThreadLocal<SimpleDateFormat> threadLocalObject();

    DateFormatEnum(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

    public static SimpleDateFormat getSimpleDateFormat(String pattern) {
        for (DateFormatEnum format : DateFormatEnum.values()) {
            if (format.pattern.equals(pattern)) {
                return format.threadLocalObject().get();
            }
        }
        LOGGER.warn("日期格式化枚举中没有找到对应的格式：{}", pattern);
        return DATE_TIME_PATTERN.threadLocalObject().get();
    }

}

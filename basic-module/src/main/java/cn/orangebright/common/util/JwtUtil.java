package cn.orangebright.common.util;

import java.security.Key;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * JWT工具类
 * @author orangebright
 * @date 2022/8/13
 */
public class JwtUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtUtil.class);

    /**
     * 签名加密算法
     */
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    /**
     * 默认密钥
     */
    private static final String DEFAULT_SECRET = "yLwfooXbIcvKiKHHrsNXLPP7pTneFbxE0A2oPMoHChw";

    /**
     * 默认过期时间：30 分钟
     */
    private static final long DEFAULT_EXPIRE_TIME = 30 * 60;

    /**
     * 密钥最短长度（测试发现HS256算法对应密钥的最短长度为43）
     */
    private static final int SECRET_MIN_LENGTH = 43;


    /**
     * 生成无过期时间的token
     * @param claims token载荷
     * @param secret 密钥
     * @return token
     */
    public static String createNoExpirationToken(Map<String, Object> claims, String secret) {
        return createToken(claims, secret, null);
    }

    /**
     * 使用默认密钥和默认过期时间生成token
     * @param claims token载荷
     * @return token
     */
    public static String createToken(Map<String, Object> claims) {
        return createToken(claims, DEFAULT_SECRET, DEFAULT_EXPIRE_TIME);
    }

    /**
     * 生成token
     * @param claims     token载荷
     * @param secret     密钥
     * @param expireTime 有效期，单位：秒
     * @return token
     */
    public static String createToken(Map<String, Object> claims, String secret, long expireTime) {
        return createToken(claims, secret, getExpireDate(expireTime));
    }

    /**
     * 生产token
     * @param claims     token载荷
     * @param secret     密钥
     * @param expireTime 有效期
     * @return token
     */
    public static String createToken(Map<String, Object> claims, String secret, Date expireTime) {
        claims = claims == null ? Collections.emptyMap() : claims;
        return Jwts.builder()
            .setClaims(claims)
            .setExpiration(expireTime)
            .setIssuer("OMaster")
            .signWith(generateKey(secret), SIGNATURE_ALGORITHM)
            .compact();
    }

    /**
     * 获取token过期时间
     * @param expireTime 有效期，单位：秒
     * @return 过期时间
     */
    private static Date getExpireDate(long expireTime) {
        return new Date(System.currentTimeMillis() + expireTime * 1000);
    }

    /**
     * 生成Key对象
     * @param secret 密钥
     * @return
     */
    private static Key generateKey(String secret) {
        if (!StringUtils.hasText(secret)) {
            secret = DEFAULT_SECRET;
        }
        if (secret.length() < SECRET_MIN_LENGTH) {
            secret += DEFAULT_SECRET.substring(0, SECRET_MIN_LENGTH - secret.length());
        }
        byte[] decode = Base64.getDecoder().decode(secret);
        return new SecretKeySpec(decode, 0, decode.length, "HmacSHA256");
    }

    public static Claims verifyToken(String token) {
        return verifyToken(token, DEFAULT_SECRET);
    }

    /**
     * 解析token，异常逻辑由调用方处理
     * @param token  token
     * @param secret 密钥
     * @return token载荷
     */
    public static Claims verifyToken(String token, String secret) {
        return Jwts.parserBuilder()
            .setSigningKey(generateKey(secret))
            .build()
            .parseClaimsJws(token)
            .getBody();
    }
}

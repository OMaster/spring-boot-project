package cn.orangebright.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 图片工具类
 *
 * @author orangebright
 * @date 2023/6/5
 */
public class ImageUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUtil.class);

    /**
     * 已指定缩放率和压缩率压缩图片
     * @param in    图片输入流
     * @param scale 缩放率
     * @param quality   压缩率
     * @return
     * @throws IOException
     */
    public static byte[] compress(InputStream in, double scale, float quality) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Thumbnails.of(in).scale(scale).outputQuality(quality).toOutputStream(baos);
        return baos.toByteArray();
    }

    /**
     * 已指定缩放率和压缩率压缩图片
     * @param picture   图片字节数组
     * @param scale 缩放率
     * @param quality   压缩率
     * @return
     * @throws IOException
     */
    public static byte[] compress(byte[] picture, double scale, float quality) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Thumbnails.of(new ByteArrayInputStream(picture))
                .scale(scale)               // 设置图片大小比例
                .outputQuality(quality)     // 设置输出质量（压缩率）
                .toOutputStream(baos);
        return baos.toByteArray();
    }

    /***
     * 将图片已指定压缩了压缩到1080P长宽
     * @param picture   图片字节数组
     * @param quality   压缩率
     * @return
     * @throws IOException
     */
    public static byte[] compress1080(byte[] picture, float quality) throws IOException {
        return compress(picture, 1920, 1080, quality);
    }

    /**
     * 将图片以指定压缩率压缩到指定长宽
     * @param picture   图片字节数组
     * @param width     压缩后的长度
     * @param height    压缩后的宽度
     * @param quality   压缩质量（压缩率）：0-1，压缩率越高，图片约清晰
     * @return  压缩后的图片字节数组
     * @throws IOException
     */
    public static byte[] compress(byte[] picture, int width, int height, float quality) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
            Thumbnails.of(new ByteArrayInputStream(picture))
                    .size(width, height)        // 设置图片长、宽
                    .outputQuality(quality)     // 设置输出质量（压缩率）
                    .toOutputStream(baos);
            return baos.toByteArray();
        } catch (IOException e) {
            LOGGER.error("图片压缩异常", e);
        }
        return null;
    }

}

package cn.orangebright.common.util;

import java.util.regex.Pattern;

/**
 * 正则工具类
 * @author snowwalker
 * @date 2024/2/27
 */
public class RegexUtil {

    /**
     * 手机号正则：匹配13、14、15、17、18、19开头的11为手机号
     */
    public static final String PHONE_REGEX = "(13|14|15|17|18|19)[0-9]{9}";
    private static final Pattern PHONE_PATTERN = Pattern.compile(PHONE_REGEX);

    /**
     * 邮箱正则：最小匹配单元：a@bb.cc
     * <ol>
     *     <li>\w：匹配任何字母数字字符（等同于 [a-zA-Z0-9_]）</li>
     *     <li>[-\w.+]：匹配 -、任意字母数字字符、. 或 +</li>
     *     <li>*：重复0次或多次</li>
     *     <li>([A-Za-z0-9][-A-Za-z0-9]+\.)+：匹配至少两个字符，第一个必须为数字或字母，第二个或后续可以为数字或字母或.或-
     *         <ol>
     *             <li>[A-Za-z0-9]：匹配单个字母或数字</li>
     *             <li>[-A-Za-z0-9]+：匹配一个或多个字母数字字符或-</li>
     *             <li>\.：匹配点号</li>
     *             <li>+：重复1次或多次（适用于多级域名）</li>
     *         </ol>
     *     </li>
     *     <li>[A-Za-z]{2,14}：匹配至少两位字母</li>
     * </ol>
     */
    public static final String EMAIL_REGEX = "\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    /**
     * 判断是否为合法手机号
     * @param phone 待校验手机号
     * @return true：合法；false：不合法
     */
    public static boolean isValidPhone(String phone) {
        return PHONE_PATTERN.matcher(phone).matches();
    }

    /**
     * 判断是否为合法邮箱
     * @param email 待校验邮箱
     * @return true：合法；false：不合法
     */
    public static boolean isValidEmail(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }
}

package cn.orangebright.common.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.util.Assert;

/**
 * Base64 编码和解码工具
 *
 * @author snowwalker
 * @date 2023/6/16
 */
public class Base64Util {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private Base64Util() {
    }

    /**
     * 将字节数组进行 Base64 编码
     *
     * @param src 源字节数组
     * @return 编码的字节数组
     */
    public static byte[] encode(byte[] src) {
        Assert.notNull(src, "源字节数组不能为null");
        if (src.length == 0) {
            return src;
        }
        return Base64.getEncoder().encode(src);
    }

    /**
     * 将给定字符串进行 Base64 编码
     *
     * @param src 源字符串
     * @return 编码后的字节数组
     */
    public static byte[] encode(String src) {
        Assert.hasText(src, "源字符串不能为空");
        return encode(getBytes(src));
    }

    /**
     * 将给定字节数组进行 Base64 编码
     *
     * @param src 源字节数组
     * @return 编码字符串
     */
    public static String encodeToString(byte[] src) {
        Assert.notNull(src, "字节数组不能为null");
        return new String(encode(src), DEFAULT_CHARSET);
    }

    /**
     * 将给定字符串进行 Base64 编码
     *
     * @param src 源字符串
     * @return 编码字符串
     */
    public static String encodeToString(String src) {
        Assert.hasText(src, "编码字符串必须有值");
        return encodeToString(getBytes(src));
    }

    /**
     * 对给定的字节数组进行 Base64 解码
     *
     * @param src 编码的字节数组
     * @return 原始字节数组
     */
    public static byte[] decode(byte[] src) {
        Assert.notNull(src, "字节数组不能为null");
        if (src.length == 0) {
            return src;
        }
        return Base64.getDecoder().decode(src);
    }

    /**
     * 将给定字符串进行 Base64 解码
     *
     * @param src 源字符串
     * @return 原始字节数组
     */
    public static byte[] decode(String src) {
        Assert.hasText(src, "编码字符串必须有值");
        return decode(getBytes(src));
    }

    /**
     * 将给定字符串使用 Base64 解码为字符串
     *
     * @param src 编码字符串
     * @return 解码后的字符串
     */
    public static String decodeToString(String src) {
        Assert.hasText(src, "编码字符串不能为空");
        return new String(decode(src), DEFAULT_CHARSET);
    }

    private static byte[] getBytes(String src) {
        return src.getBytes(DEFAULT_CHARSET);
    }
}

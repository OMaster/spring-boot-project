package cn.orangebright.common.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * 日期时间工具类
 *
 *  <li>LocalDate：日期值对象如2019-09-22</li>
 *  <li>LocalTime：时间值对象如21:25:36</li>
 *  <li>LocalDateTime：日期+时间值对象</li>
 *  <li>Zoned：时区</li>
 *  <li>ZonedDateTime：日期+时间+时区值对象</li>
 *  <li>DateTime Formatter：用于日期时间的格式化</li>
 *  <li>Period：用于计算日期间隔</li>
 *  <li>Duration：用于计算时间间隔</li>
 *
 * @author orangebright
 * @date 2021/12/16
 */
public class DateTimeUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtil.class);

    /**
     * 获取系统默认时区
     */
    public static final ZoneId TIME_ZONE = ZoneId.systemDefault();

    /**
     * 默认字符串格式
     */
    public static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * SimpleDateFormat 类型的 ThreadLocal
     */
    @Deprecated
    public static final ThreadLocal<SimpleDateFormat> SDF_THREAD_LOCAL = ThreadLocal.withInitial(() -> new SimpleDateFormat(DEFAULT_PATTERN));

    /**
     * 获取 SimpleDateFormat
     * @return  ThreadLocal存储的 SimpleDateFormat对象
     */
    @Deprecated
    public static SimpleDateFormat getSimpleDateFormat() {
        return SDF_THREAD_LOCAL.get();
    }

    /**
     * 将Date类型转为LocalDateTime类型
     * @param date  Date日期类型
     * @return  LocalDateTime类型
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        return LocalDateTime.ofInstant(date.toInstant(), TIME_ZONE);
    }

    /**
     * 将LocalDateTime对象转换为Date对象
     * @param localDateTime LocalDateTime对象
     * @return  Date对象
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Date.from(localDateTime.atZone(TIME_ZONE).toInstant());
    }

    /**
     * 使用默认格式格式化Date对象
     * @param date Date对象
     * @return  {@link DateTimeUtil#DEFAULT_PATTERN} 格式的字符串
     */
    public static String dateFormat(Date date) {
        return dateFormat(date, DEFAULT_PATTERN);
    }

    /**
     * 使用指定pattern格式化Date对象
     * @param date Date对象
     * @param pattern   日期时间格式
     * @return  格式化的字符串形式的日期时间
     */
    public static String dateFormat(Date date, String pattern) {
        if (date == null || !StringUtils.hasText(pattern)) {
            return "";
        }
        return DateFormatEnum.getSimpleDateFormat(pattern).format(date);
    }

    /**
     * 使用默认格式格式化LocalDateTime对象
     * @param localDateTime LocalDateTime对象
     * @return  {@link DateTimeUtil#DEFAULT_PATTERN} 格式的字符串
     */
    public static String localDateTimeFormat(LocalDateTime localDateTime) {
        return localDateTimeFormat(localDateTime, DEFAULT_PATTERN);
    }

    /**
     * 使用指定pattern格式化LocalDateTime对象
     * @param localDateTime LocalDateTime对象
     * @param pattern   日期时间格式
     * @return  格式化的字符串形式的日期时间
     */
    public static String localDateTimeFormat(LocalDateTime localDateTime, String pattern) {
        if (localDateTime == null || !StringUtils.hasText(pattern)) {
            return "";
        }
        return DateTimeFormatter.ofPattern(pattern).format(localDateTime);
    }

    /**
     * 将字符串解析为Date
     * @param source    日期时间字符串
     * @param pattern   解析格式
     * @return  Date对象
     */
    public static Date parseToDate(String source, String pattern) {
        if (!StringUtils.hasText(source) || !StringUtils.hasText(pattern)) {
            return null;
        }
        try {
            return DateFormatEnum.getSimpleDateFormat(pattern).parse(source);
        } catch (ParseException e) {
            LOGGER.error("字符串{}使用{}解析错误，请查找原因！", source, pattern, e);
        }
        return null;
    }

    /**
     * 将字符串解析为LocalDateTime
     * @param source    日期时间字符串
     * @param pattern   解析格式
     * @return  LocalDateTime对象
     */
    // TODO: 2021/12/17 待完善
    public static LocalDateTime parseToLocalDateTime(String source, String pattern) {
        if (!StringUtils.hasText(source) || !StringUtils.hasText(pattern)) {
            return null;
        }
        return LocalDateTime.parse(source, DateTimeFormatter.ofPattern(pattern));
    }

}

package cn.orangebright.common.util;

import java.text.DecimalFormat;

/**
 * 数字工具类
 *
 * @author orangebright
 * @date 2021/12/15
 */
public class NumberUtil {

    /**
     * 将浮点数转换为保留两位小数的百分比字符串
     * @param data  浮点数
     * @return  百分比字符串
     */
    public static String getPercentage(double data) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00%");
        return decimalFormat.format(data);
    }
}

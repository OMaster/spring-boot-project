package cn.orangebright.common.util;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 地区工具类
 *
 * @author snowwalker
 * @date 2023/7/3
 */
public class RegionUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionUtil.class);

    /**
     * 未知地址
     */
    public static final String UNKNOWN = "XX XX";

    /**
     * IP查询地址
     */
    public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp?json=true&ip=";

    /**
     * 获取ip对应的地区
     * @param ip    ip地址
     * @return  省市二级地区
     */
    public static String getRegion(String ip) {
        try {
            // TODO: 2023/7/3 内网ip，返回内网IP
            String result = HttpClientUtil.get(IP_URL + ip);
            if (StrUtil.isEmpty(result)) {
                LOGGER.error("获取地理位置异常 {}" , ip);
                return UNKNOWN;
            }
            JsonNode root = JsonUtil.jsonToTree(result);
            String pro = root.get("pro").textValue();
            String city = root.get("city").textValue();
            System.out.println(pro + " " + city);
            return String.format("%s %s", pro, city);
        } catch (Exception e) {
            LOGGER.error("");
            return UNKNOWN;
        }
    }

}
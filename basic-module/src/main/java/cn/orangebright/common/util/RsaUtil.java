package cn.orangebright.common.util;

import java.security.KeyPair;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.AsymmetricAlgorithm;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * RSA工具类
 * @author orangebright
 * @date 2023/6/16
 */
public class RsaUtil {

    /**
     * 默认加密算法
     */
    public static final String DEFAULT_RSA_ALGORITHM = AsymmetricAlgorithm.RSA_ECB_PKCS1.getValue();

    /**
     * 生成公钥和私钥
     * @return  公钥和私钥
     */
    public static Base64KeyPair generateKeyPair() {
        return generateKeyPair(null);
    }

    /**
     * 生成公钥和私钥
     * @param algorithm 生成密钥的算法，默认"RSA"
     * @return  公钥和私钥
     */
    public static Base64KeyPair generateKeyPair(String algorithm) {
        if (algorithm == null) {
            algorithm = DEFAULT_RSA_ALGORITHM;
        }
        KeyPair keyPair = SecureUtil.generateKeyPair(algorithm);
        return Base64KeyPair.from(keyPair);
    }

    /**
     * 使用公钥加密
     * @param content   要加密的内容
     * @param publicKey Base64的公钥
     * @return  Base64的加密字符串
     */
    public static String encrypt(String content, String publicKey) {
        RSA rsa = new RSA(null, publicKey);
        return rsa.encryptBase64(content, KeyType.PublicKey);
    }

    /**
     * 使用私钥解密
     * @param content   Base64的加密字符串
     * @param privateKey    Base64的私钥
     * @return  加密的原始内容
     */
    public static String decrypt(String content, String privateKey) {
        RSA rsa = new RSA(privateKey, null);
        return rsa.decryptStr(content, KeyType.PrivateKey);
    }

    public static class Base64KeyPair {
        private String publicKey;
        private String privateKey;

        public Base64KeyPair(String publicKey, String privateKey) {
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }

        public static Base64KeyPair from(KeyPair keyPair) {
            String publicKey = Base64Util.encodeToString(keyPair.getPublic().getEncoded());
            String privateKey = Base64Util.encodeToString(keyPair.getPrivate().getEncoded());
            return new Base64KeyPair(publicKey, privateKey);
        }

        public String getPublicKey() {
            return publicKey;
        }

        public String getPrivateKey() {
            return privateKey;
        }

    }
}


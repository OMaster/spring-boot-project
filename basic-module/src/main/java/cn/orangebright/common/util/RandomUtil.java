package cn.orangebright.common.util;

import java.security.SecureRandom;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机数工具类
 *
 * @author orangebright
 * @date 2021/12/2
 */
public class RandomUtil {

    /**
     * SecureRandom对象的单例，是否有必要
     */
    public static class SecureRandomHolder {
        public static final SecureRandom secureRandom = getSecureRandom();
    }

    /**
     * 获取安全随机数对象
     *
     * @return
     */
    public static SecureRandom getSecureRandom() {
        return new SecureRandom();
    }

    /**
     * 获取随机数生成器对象<br>
     * ThreadLocalRandom是JDK 7之后提供并发产生随机数，能够解决多个线程发生的竞争争夺。
     *
     * @return {@link ThreadLocalRandom}
     */
    public static ThreadLocalRandom getRandom() {
        return ThreadLocalRandom.current();
    }

    /**
     * 获取随机int数
     *
     * @return
     */
    public static int randomInt() {
        return SecureRandomHolder.secureRandom.nextInt();
    }

    /**
     * 获取0~max-1范围内的随机数 [0, max - 1]，即 [0, max)
     *
     * @param max 可以获取到的随机数的最大值（不包括）
     * @return
     */
    public static int randomInt(int max) {
        return SecureRandomHolder.secureRandom.nextInt(max);
    }

    /**
     * 获取指定范围内的随机数 [min, max)
     *
     * @param min 范围下限（包括）
     * @param max 范围上限（不包括）
     * @return
     */
    public static int randomInt(int min, int max) {
        return min + SecureRandomHolder.secureRandom.nextInt(max - min);
    }

}

package cn.orangebright.common.util.jackson;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * ObjectMapper 对象持有类
 * <p>configure()配置方法分两类：
 * <li>序列化配置（对象 --> json串）：详见{@link ObjectMapper#configure(SerializationFeature, boolean)}和{@link DeserializationConfig}</li>
 * <li>反序列化配置（json串 --> 对象）：详见{@link ObjectMapper#configure(DeserializationFeature, boolean)}和{@link SerializationConfig}</li>
 * @author orangebright
 * @date 2023/4/20
 */
public class ObjectMapperUtil {

    public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 每次创建新对象
     * <p>创建新对象后，立即完成自定义配置，然后进行操作；如果操作（如序列化对象）后，再进行配置不会生效
     * <p>原因：一旦使用映射器，由于序列化器和反序列化器的缓存，并非所有设置都生效。
     * 配置一次实例，第一次使用后不要更改设置。 它通过这种方式实现线程安全性和性能。
     * @return 新创建的 ObjectMapper 对象
     */
    public static ObjectMapper create() {
        ObjectMapper objectMapper = new ObjectMapper();
        defaultConfigure(objectMapper);
        addJavaTimeSupport(objectMapper);
        return objectMapper;
    }

    public static void defaultConfigure(ObjectMapper objectMapper) {
        // 指定要序列化的域：PropertyAccessor.ALL：所有字段；JsonAutoDetect.Visibility.ANY：所有访问修饰符
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

        // 解析json串时（反序列化）遇到未定义的属性时是否抛出JsonMappingException异常，默认为true
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // 配置特殊字符（值小于32的ASCII字符）的解析（如：'\r\n'）
        objectMapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
    }

    /**
     * 增加对{@link LocalDateTime}序列化及反序列化的支持，并格式化为{@code yyyy-MM-dd HH:mm:ss}
     * <p>ps：若需要增加对{@link java.time.LocalDate} 等java.time包的其他类型的支持，参照这同步添加相应的序列化期和反序列化器即可
     * @param objectMapper
     * @see JavaTimeModule
     * @see LocalDateTimeSerializer
     * @see LocalDateTimeDeserializer
     */
    public static void addJavaTimeSupport(ObjectMapper objectMapper) {
        JavaTimeModule javaTimeModule = new JavaTimeModule();

        // 指定序列化、反序列化的LocalDateTime字符串格式，默认为ISO格式
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dtf));
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dtf));

        objectMapper.registerModule(javaTimeModule);
    }


    /**
     * 将对象全限定名称添加到序列化结果中，和对象的序列化结果组成一个数组（常用于redis的序列化）
     * <p>{@link ObjectMapper#getPolymorphicTypeValidator()}的值详见{@link ObjectMapper#DEFAULT_BASE}
     * @param objectMapper
     */
    public static void addClassName(ObjectMapper objectMapper) {
        // 指定序列化输入的类型（将对象全限定名称添加到序列化结果中）：
        //  JsonTypeInfo.As.PROPERTY——对象类型最为一个属性；
        //  JsonTypeInfo.As.WRAPPER_ARRAY——对象类型和对象组成一个数组
        objectMapper.activateDefaultTyping(objectMapper.getPolymorphicTypeValidator(), ObjectMapper.DefaultTyping.NON_FINAL);
    }

    /**
     * 排除null字段，只序列化非null字段
     * @param objectMapper
     */
    public static void excludeNullField(ObjectMapper objectMapper) {
        // 置顶序列化规则：只序列化值不为null的属性
        // Include.Include.ALWAYS 默认
        // Include.NON_DEFAULT 属性为默认值不序列化
        // Include.NON_EMPTY 属性为空（""）或者为NULL都不序列化，返回的json是没有这个字段的。
        // Include.NON_NULL 属性为NULL的字段不序列化
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}

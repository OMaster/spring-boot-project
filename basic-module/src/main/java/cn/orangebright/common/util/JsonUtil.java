package cn.orangebright.common.util;

import java.io.IOException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.exception.ServiceException;
import cn.orangebright.common.util.jackson.ObjectMapperUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Json工具类
 * @author orangebright
 * @date 2021/12/1
 */
public class JsonUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

    /***********************************************************************************************
     *                              Jackson 操作
     ***********************************************************************************************/

    private static final ObjectMapper objectMapper = ObjectMapperUtil.create();

    /**
     * 将对象转换为简洁样式的json串（一行）
     * @param object java对象
     * @return json
     */
    public static String objectToString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("{}转换为json字符串出错", object, e);
            throw new ServiceException("json转换错误", e);
        }
    }

    /**
     * 将java对象转换为格式化的json串（格式化，有换行、缩进）
     * @param object java对象
     * @return json
     */
    public static String objectToFormatString(Object object) {
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("{}转换为json字符串出错", object, e);
            throw new ServiceException("json转换错误", e);
        }
    }

    /**
     * 将对象转换为json串，并写入到 response中
     * @param object   对象
     * @param response response
     */
    public static void writeObjectToResponse(Object object, HttpServletResponse response) {
        response.setContentType("application/json; charset=UTF-8");
        try (ServletOutputStream sos = response.getOutputStream()) {
            objectMapper.writeValue(sos, object);
            sos.flush();
        } catch (IOException e) {
            LOGGER.error("{}转换为json字符串出错", object, e);
            throw new ServiceException("json转换错误", e);
        }
    }

    /**
     * 将json串转换为java对象
     * @param json  json串
     * @param clazz java对象
     * @param <T>   java对象
     * @return java对象
     */
    public static <T> T jsonToObject(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            LOGGER.error("{}反序列化为{}对象出错", json, clazz.getName(), e);
            throw new ServiceException("反序列化失败", e);
        }
    }

    /**
     * 将json串转换为json树
     * @param json json串
     * @return JsonNode对象
     */
    public static JsonNode jsonToTree(String json) {
        try {
            return objectMapper.readTree(json);
        } catch (JsonProcessingException e) {
            LOGGER.error("{}转换为JsonNode对象出错", json, e);
            throw new ServiceException("json转换错误", e);
        }
    }

    /**
     * 将java对象转换为json树：JsonNode对象
     * <p>
     * 等价于 objectMapper.convertValue(obj, JsonNode.class)
     * @param obj java对象
     * @return JsonNode对象
     */
    public static JsonNode objectToTree(Object obj) {
        return objectMapper.valueToTree(obj);
    }

    /**
     * 将JsonNode对象转换为java对象
     * <p>
     * 等价于 objectMapper.convertValue(jsonNode, clazz)
     * @param jsonNode JsonNode对象
     * @param clazz    java对象
     * @param <T>      java对象
     * @return java对象
     */
    public static <T> T treeToObject(JsonNode jsonNode, Class<T> clazz) {
        try {
            return objectMapper.treeToValue(jsonNode, clazz);
        } catch (JsonProcessingException e) {
            LOGGER.error("{}转换为{}对象出错", jsonNode, clazz.getName(), e);
            throw new ServiceException("json转换错误", e);
        }
    }
}

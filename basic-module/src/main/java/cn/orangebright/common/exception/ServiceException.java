package cn.orangebright.common.exception;

import java.io.Serial;

/**
 * 业务异常
 * @author snowwalker
 * @date 2023/8/14
 */
public class ServiceException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 9161710049543601914L;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}

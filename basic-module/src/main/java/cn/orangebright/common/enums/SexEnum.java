package cn.orangebright.common.enums;

/**
 * 性别枚举
 *
 * @author snowwalker
 * @date 2023/10/10
 */
public enum SexEnum {
    /**
     * 男
     */
    MAN("1", "男"),

    /**
     * 女
     */
    WOMAN("2", "女"),

    /**
     * 未知（未知和空的区别，是否需要未知）
     */
    UNKNOWN("3", "未知"),
    ;

    private final String code;
    private final String value;

    SexEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}

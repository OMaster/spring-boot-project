package cn.orangebright.common.captcha;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.imageio.ImageIO;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import cn.orangebright.common.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * 图形验证码工具类
 *
 * @author orangebright
 * @date 2021/12/2
 */
public class CaptchaUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaptchaUtil.class);

    /**
     * 使用到Algerian字体，系统里没有的话需要安装字体，字体只显示大写，去掉了1,0,i,o几个容易混淆的字符
     */
    private static final String VERIFY_CODES = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";

    /**
     * 图片转base64数据格式
     */
    public static final String BASE64_HEAD = "data:image/jpeg;base64,";

    public static Base64Captcha outputBase64Image() {
        return outputBase64Image(null, null);
    }

    /**
     * 将生成的验证码转换为base64返回
     * @param captcha   验证码实体对象
     * @return
     */
    public static Base64Captcha outputBase64Image(CaptchaProperties captcha) {
        return outputBase64Image(captcha, null);
    }

    /**
     * 将生成的验证码转换为base64返回
     * @param captcha   验证码实体对象
     * @param sources   验证码资源串
     * @return
     */
    public static Base64Captcha outputBase64Image(CaptchaProperties captcha, String sources) {
        if (captcha == null) {
            captcha = new CaptchaProperties();
        }
        String verifyCode = generateVerifyCode(captcha.getSize(), sources);
        BufferedImage bufferedImage = outputImage(captcha.getWidth(), captcha.getHeight(), verifyCode);
        try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, "jpg", outputStream);
            String base64Image = BASE64_HEAD + Base64.getEncoder().encodeToString(outputStream.toByteArray());
            return new Base64Captcha(verifyCode, base64Image);
        } catch (IOException e) {
            LOGGER.error("生成图形验证码出错！", e);
            return null;
        }
    }

    /**
     * 将生成的验证码写入到输出流中
     * @param response  response对象
     * @return  验证码
     */
    public static String outputVerifyImage(HttpServletResponse response) {
        return outputVerifyImage(null, null, response);
    }

    /**
     * 将生成的验证码写入到输出流中
     * @param captcha   验证码实体对象
     * @param response  response对象
     * @return  验证码
     */
    public static String outputVerifyImage(CaptchaProperties captcha, HttpServletResponse response) {
        return outputVerifyImage(captcha, null, response);
    }

    /**
     * 将生成的验证码写入到输出流中
     * @param captcha   验证码实体对象
     * @param sources   验证码资源串
     * @param response  response对象
     * @return  验证码
     */
    public static String outputVerifyImage(CaptchaProperties captcha, String sources, HttpServletResponse response) {
        if (captcha == null) {
            captcha = new CaptchaProperties();
        }
        String verifyCode = generateVerifyCode(captcha.getSize(), sources);
        response.setContentType("image/jpg");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        try (ServletOutputStream os = response.getOutputStream())  {
            BufferedImage bufferedImage = outputImage(captcha.getWidth(), captcha.getHeight(), verifyCode);
            ImageIO.write(bufferedImage, "jpg", os);
        } catch (IOException e) {
            LOGGER.error("生成图形验证码出错！", e);
        }
        return verifyCode;
    }

    /**************************************************************************************************
     *                          第一部分：生成 N 位随机字符串
     *************************************************************************************************/

    /**
     * 使用指定源生成验证码
     * @param verifySize    验证码长度
     * @param sources       验证码字符源
     * @return  验证码
     */
    private static String generateVerifyCode(int verifySize, String sources){
        if (!StringUtils.hasText(sources)) {
            sources = VERIFY_CODES;
        }
        SecureRandom random = RandomUtil.getSecureRandom();
        int codesLen = sources.length();
        StringBuilder verifyCode = new StringBuilder(verifySize);
        for(int i = 0; i < verifySize; i++){
            verifyCode.append(sources.charAt(random.nextInt(codesLen-1)));
        }
        return verifyCode.toString();
    }

    /**************************************************************************************************
     *                          第二部分：生成图片样式
     *************************************************************************************************/

    /**
     * 输出指定验证码图片流
     * @param w     图片宽
     * @param h     图片高
     * @param code  验证码
     */
    private static BufferedImage outputImage(int w, int h, String code) {
        SecureRandom random = RandomUtil.getSecureRandom();
        int verifySize = code.length();
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        // 设置边框色
        g2.setColor(Color.GRAY);
        g2.fillRect(0, 0, w, h);
        Color c = getRandColor(200, 250);
        // 设置背景色
        g2.setColor(c);
        g2.fillRect(0, 2, w, h-4);
        //绘制干扰线
        g2.setColor(getRandColor(160, 200));
        for (int i = 0; i < 20; i++) {
            int x = random.nextInt(w - 1);
            int y = random.nextInt(h - 1);
            int xl = random.nextInt(6) + 1;
            int yl = random.nextInt(12) + 1;
            g2.drawLine(x, y, x + xl + 40, y + yl + 20);
        }
        // 使图片扭曲
        shear(g2, w, h, c);
        g2.setColor(getRandColor(100, 160));
        int fontSize = h-4;
        Font font = new Font("Algerian", Font.ITALIC, fontSize);
        g2.setFont(font);
        char[] chars = code.toCharArray();
        for(int i = 0; i < verifySize; i++){
            AffineTransform affine = new AffineTransform();
            affine.setToRotation(Math.PI / 4 * random.nextDouble() * (random.nextBoolean() ? 1 : -1),
                    (w / verifySize) * i + fontSize / 2, h / 2);
            g2.setTransform(affine);
            g2.drawChars(chars, i, 1, ((w-10) / verifySize) * i + 5, h/2 + fontSize/2 - 10);
        }
        g2.dispose();
        // 添加噪点
        float yawpRate = 0.05f;
        int area = (int) (yawpRate * w * h);
        for (int i = 0; i < area; i++) {
            int x = random.nextInt(w);
            int y = random.nextInt(h);
            int rgb = getRandomIntColor();
            image.setRGB(x, y, rgb);
        }
        return image;
    }

    private static Color getRandColor(int fc, int bc) {
        fc = Math.min(fc, 255);
        bc = Math.min(bc, 255);
        SecureRandom random = RandomUtil.getSecureRandom();
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    private static int getRandomIntColor() {
        int[] rgb = getRandomRgb();
        int color = 0;
        for (int c : rgb) {
            color = color << 8;
            color = color | c;
        }
        return color;
    }

    private static int[] getRandomRgb() {
        SecureRandom random = RandomUtil.getSecureRandom();
        int[] rgb = new int[3];
        for (int i = 0; i < 3; i++) {
            rgb[i] = random.nextInt(255);
        }
        return rgb;
    }

    private static void shear(Graphics g, int w1, int h1, Color color) {
        shearX(g, w1, h1, color);
        shearY(g, w1, h1, color);
    }
    private static void shearX(Graphics g, int w1, int h1, Color color) {
        int period = RandomUtil.randomInt(2);
        int frames = 1;
        int phase = RandomUtil.randomInt(2);
        for (int i = 0; i < h1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(0, i, w1, 1, (int) d, 0);
            g.setColor(color);
            g.drawLine((int) d, i, 0, i);
            g.drawLine((int) d + w1, i, w1, i);
        }
    }
    private static void shearY(Graphics g, int w1, int h1, Color color) {
        int period = RandomUtil.randomInt(10, 40);
        int frames = 20;
        int phase = 7;
        for (int i = 0; i < w1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(i, 0, 1, h1, 0, (int) d);
            g.setColor(color);
            g.drawLine(i, (int) d, i, 0);
            g.drawLine(i, (int) d + h1, i, h1);
        }
    }
}

package cn.orangebright.common.captcha;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Base64图片
 *
 * @author orangebright
 * @date 2022/6/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Base64Captcha {

    /**
     * 验证码的key值
     */
    private String key;

    /**
     * 生成的验证码，或验证码的key值
     */
    private String code;

    /**
     * 图片验证码的base64编码
     */
    private String captcha;

    public Base64Captcha(String code, String captcha) {
        this.code = code;
        this.captcha = captcha;
    }
}

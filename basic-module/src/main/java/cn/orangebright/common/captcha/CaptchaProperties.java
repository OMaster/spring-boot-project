package cn.orangebright.common.captcha;

import java.io.Serial;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 验证码属性
 *
 * @author orangebright
 * @date 2021/12/2
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaptchaProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = 1076795777705763323L;

    /**
     * 验证码宽度，默认100
     */
    private int width = 100;

    /**
     * 验证码高度，默认40
     */
    private int height = 40;

    /**
     * 验证码长度，默认4
     */
    private int size = 4;
}

package cn.orangebright.common.response;

/**
 * 统一接口返回状态
 *
 * @author orangebright
 * @date 2021/12/1
 */
public enum ResultStatus {

    /**
     * 成功
     */
    SUCCESS(200, "操作成功"),

    /**
     * 失败
     */
    FAILURE(400, "操作失败"),

    /**
     * 未认证
     */
    UNAUTHORIZED(401, "暂未登录或token已经过期"),

    /**
     * 未授权
     */
    FORBIDDEN(403, "无访问权限"),

    /**
     * 服务器错误
     */
    SERVER_ERROR(500, "服务异常，请刷新页面重试")
    ;

    private final int code;

    private final String defaultMessage;

    ResultStatus(int code, String defaultMessage) {
        this.code = code;
        this.defaultMessage = defaultMessage;
    }

    public int getCode() {
        return code;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }
}

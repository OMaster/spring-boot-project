package cn.orangebright.common.response;

import java.io.Serial;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一接口返回对象
 * <p>
 * 1、对于前端调用的接口，可以使用静态方法
 * <p>
 * 2、对于其他系统调用的接口，必须使用构造器指定接口返回的数据类型
 *
 * @author orangebright
 * @date 2021/12/1
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultBack<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = -6577244805142936206L;

    private int code;

    private String message;

    private T data;

    public ResultBack() {
    }

    public ResultBack(ResultStatus resultStatus) {
        this.code = resultStatus.getCode();
        this.message = resultStatus.getDefaultMessage();
    }

    public ResultBack(ResultStatus resultStatus, String message) {
        this(resultStatus, message, null);
    }

    public ResultBack(ResultStatus resultStatus, String message, T data) {
        this.code = resultStatus.getCode();
        this.message = message;
        this.data = data;
    }


    // ================================================ success

    public static <T> ResultBack<T> success() {
        return new ResultBack<>(ResultStatus.SUCCESS);
    }

    public static <T> ResultBack<T> success(String message) {
        return success(message, null);
    }

    public static <T> ResultBack<T> success(T data) {
        return success(null, data);
    }

    public static <T> ResultBack<T> success(String message, T data) {
        return new ResultBack<>(ResultStatus.SUCCESS, message, data);
    }

    // ================================================ failure

    public static <T> ResultBack<T> failure() {
        return new ResultBack<>(ResultStatus.FAILURE);
    }

    public static <T> ResultBack<T> failure(String message) {
        return new ResultBack<>(ResultStatus.FAILURE, message);
    }

    public static <T> ResultBack<T> failure(T data) {
        return failure(null, data);
    }

    public static <T> ResultBack<T> failure(String message, T data) {
        return new ResultBack<>(ResultStatus.FAILURE, message, data);
    }

    // ================================================ unauthorized

    public static <T> ResultBack<T> unauthorized() {
        return new ResultBack<>(ResultStatus.UNAUTHORIZED);
    }

    public static <T> ResultBack<T> unauthorized(String message) {
        return new ResultBack<>(ResultStatus.UNAUTHORIZED, message);
    }

    // ================================================ forbidden

    public static <T> ResultBack<T> forbidden() {
        return new ResultBack<>(ResultStatus.FORBIDDEN);
    }

    public static <T> ResultBack<T> forbidden(String message) {
        return new ResultBack<>(ResultStatus.FORBIDDEN, message);
    }

    // ================================================ serverError

    public static <T> ResultBack<T> serverError() {
        return new ResultBack<>(ResultStatus.SERVER_ERROR);
    }

    public static <T> ResultBack<T> serverError(String message) {
        return new ResultBack<>(ResultStatus.SERVER_ERROR, message);
    }
}

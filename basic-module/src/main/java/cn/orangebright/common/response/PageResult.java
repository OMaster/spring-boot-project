package cn.orangebright.common.response;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 分页数据实体
 * @author snowwalker
 * @date 2023/10/18
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class PageResult<T> {

    /**
     * 当前页
     */
    private long pageIndex = 1;

    /**
     * 分页大小
     */
    private long pageSize = 10;

    /**
     * 数据总数
     */
    private long total = 0;

    /**
     * 数据集
     */
    private List<T> dataList;

    public PageResult(long pageIndex, long pageSize, long total) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.total = total;
    }

}

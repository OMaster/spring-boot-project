package cn.orangebright.common.response;

import java.util.function.Function;

import cn.orangebright.common.bean.PageVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 与 Mybatis Plus 分页转换工具
 * @author snowwalker
 * @date 2024/2/19
 */
public class MpPageUtil {

    /**
     * 初始化 mybatis plus 分页对象
     * @return
     */
    public static <T> Page<T> toMpPage(PageResult<T> pageResult) {
        return Page.of(pageResult.getPageIndex(), pageResult.getPageSize());
    }

    /**
     * 初始化 mybatis plus 分页对象
     * @return
     */
    public <T> Page<T> toMpPage(PageVo pageVo) {
        return Page.of(pageVo.getPageIndex(), pageVo.getPageSize());
    }

    /**
     * 初始化 PageResult 对象
     * @param page Mp 分页对象
     * @param <T>
     * @return
     */
    public static <T> PageResult<T> from(Page<T> page) {
        PageResult<T> pageResult = new PageResult<>();
        if (page != null) {
            pageResult.setPageIndex(page.getCurrent());
            pageResult.setPageSize(page.getSize());
            pageResult.setTotal(page.getTotal());
            pageResult.setDataList(page.getRecords());
        }
        return pageResult;
    }

    public static <T, R> PageResult<R> from(Page<T> page, Function<T, R> mapper) {
        PageResult<R> pageResult = new PageResult<>();
        if (page != null) {
            pageResult.setPageIndex(page.getCurrent());
            pageResult.setPageSize(page.getSize());
            pageResult.setTotal(page.getTotal());
            pageResult.setDataList(page.getRecords().stream().map(mapper).toList());
        }
        return pageResult;
    }

}
